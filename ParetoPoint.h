#ifndef PARETOPOINT_H
#define PARETOPOINT_H

template<int nPar, int nCrit> class ParetoPoint
{
public:
    ParetoPoint();
    ParetoPoint(double* , double);
    ParetoPoint(double* params, double* crits, double delta, double satisfaction_bit);
    
    static ParetoPoint<nPar, nCrit>* FormPoint(double* , double );
    static ParetoPoint<nPar, nCrit>* FormPoint(double* params, double* crits,
					       double delta, double satisfaction_bit);

    // ��������� �������� ���������
    // criteria - ����� ���������
    void ComputeCriteria(std::vector<Function<nPar>*> &criteria);

    //! ��������� ���������� �����������
    //! \return true, ���� ����������� �����������
    bool ProveConstrains(std::vector<Function<nPar>*> &constrains);

    //! ��������� �������� �����������
    /*! ���� ���������� �������� ������ ����, ��
     *  ����������� �����������
    */
    double ComputeConstrain(Function<nPar>* constrain) {
        return constrain->Compute(params);
    }

    // ����������� ���������� ��� �����������,
    // �� ������ �������� ���������
    static int sort_crit_id;

    double params[nPar];
    double crits[nCrit];
    double delta;
    bool satisfaction_bit;
};

// ����������� ���������� �������� ��������,
// �� �������� ����� ������������� �����
template<int nPar, int nCrit>
int ParetoPoint<nPar, nCrit>::sort_crit_id = 0;

// ��������� �������� ���������
template<int nPar, int nCrit> void ParetoPoint<nPar, nCrit>::
        ComputeCriteria(std::vector<Function<nPar>*> &criteria)
{
    for(int crit_id = 0; crit_id < nCrit; crit_id++)
        crits[crit_id] = criteria[crit_id]->Compute(params);
}

// ��������� ���������� �����������
template<int nPar, int nCrit> bool ParetoPoint<nPar, nCrit>::
        ProveConstrains(std::vector<Function<nPar>*> &constrains)
{
    for(auto p : constrains)
        if(ComputeConstrain(p) < 0)
            return false;
    return true;
}

template<int nPar, int nCrit> ParetoPoint<nPar, nCrit>::
		ParetoPoint()
{
    for(int i = 0; i < nPar; ++i)
	params[i] = 0;
    for(int i = 0; i < nCrit; ++i)
	crits[i] = 0;
    satisfaction_bit = false;
}

template<int nPar, int nCrit> ParetoPoint<nPar, nCrit>::
		ParetoPoint(double* params, double* crits, double delta, double satisfaction_bit)
{
    for(int i = 0; i < nPar; ++i)
	this->params[i] = params[i];
    for(int i = 0; i < nCrit; ++i)
	this->crits[i] = crits[i];
    this->delta = delta;
    this->satisfaction_bit = satisfaction_bit;
}

template<int nPar, int nCrit> ParetoPoint<nPar, nCrit>::
		ParetoPoint(double* in_params, double in_delta)
{
    for(int i = 0; i < nPar; ++i)
	params[i] = in_params[i];
    for(int i = 0; i < nCrit; ++i)
	crits[i] = 0;
    delta = in_delta;
    satisfaction_bit = false;
}

template<int nPar, int nCrit> ParetoPoint<nPar, nCrit>* ParetoPoint<nPar, nCrit>::
		FormPoint(double* params, double* crits,
			  double delta, double satisfaction_bit)
{
    ParetoPoint<nPar, nCrit>* par_point = new ParetoPoint<nPar, nCrit>(params, crits,
							    delta, satisfaction_bit);   
    return par_point;
}

template<int nPar, int nCrit> ParetoPoint<nPar, nCrit>* ParetoPoint<nPar, nCrit>::
		FormPoint(double* in_params, double in_delta)
{
    ParetoPoint<nPar, nCrit>* par_point = new ParetoPoint<nPar, nCrit>(in_params, in_delta);   
    return par_point;
}
#endif
