#ifndef MONTEPAWSALGORITHM_H
#define MONTEPAWSALGORITHM_H

////// ��������� ��������� ////////
#include <cmath>
#include <list>
#include <memory>       // shared_ptr
#include <algorithm>    // sort
#include <vector>
///////////////////////////////////

////// ��������� ��������� ////////
#include "Function.h"
#include "ParetoPoint.h"
///////////////////////////////////

// ���������� ��� ������ ��������� �� ����� ������
typedef std::shared_ptr<ParetoPoint<NUM_PAR, NUM_CRIT>> ParetoPointer;
typedef std::shared_ptr<Function<NUM_PAR>>              FunctionPointer;


template<int nPar, int nCrit> class MontePawsAlgorithm
{
public:
    // delta    - ������ ������� ������ ������� �������
    //            �����-�����
    // iter_num - ����� �������� ������ ���������
    // monte_carlo_points_num - ����� �����,
    //            ������������ �������� ������ ������
    MontePawsAlgorithm(double delta, int iter_num,
                       int monte_carlo_points_num);

    // ���������� ��������
    void SetCriteria(std::vector<FunctionPointer> &criteria) {
        _criteria = criteria;
    }    
    // ���������� ��������
    void SetConstrains(std::vector<FunctionPointer> &constrains) {
        _constrains = constrains;
    }

    // ������ ���������
    void RunAlgorithm();

    //-----------------------------------------//
    //////***//// ��������� ������� ////***//////

    // ����� ����������� ����� (�������� -
    //                          ���������� ���������������)
    void FindCenterPoint();

    // ����� �������� �������������� ���������
    // ������� �����-�����
    void MinimizePersonalCrits();

    //-------------------------------------------

    // ��������� ��������� ���������� ����� �����
    // ��������� �������
    double _ComputeDistance(const ParetoPointer &point1,
                            const ParetoPointer &point2) const;

    // ������������� ��������� ����� ������ �������� ������;
    // �� ��� ����� ����� �������� ������
    void _GenerateRandomPoints();

    /////////////////////////////////////////////
    //=========================================//

//private:

    //////***////     ������       ////***//////
    std::list<ParetoPointer> _archive_points;    // �������� �����
    //---------------------------------------------------
    std::list<ParetoPointer> _montecarlo_points; // ��������� �����-����� �����
    std::list<ParetoPointer> _personal_mins;
    // �������� �������������� ��������� � �������� �����
    // ��������
    ParetoPointer _centerPoint = nullptr;
    // ����������� ����� ��� ������ ��������
    //---------------------------------------------------

    /////////////////////////////////////////////

    //////***//// ��������� ������ ////***//////
    double _delta    = 0;                // ������ ������� ������
    int    _iter_num = 0;                // ����� �������� ������
    int    _monte_carlo_points_num = 0;  // ����� ������������ ���������
                                         // ����� ������ ������
    /////////////////////////////////////////////

    //////***////    ��������    ////***//////
    std::vector<FunctionPointer> _criteria;

    //////***////    �����������    ////***//////
    std::vector<FunctionPointer> _constrains;
};

/////////////////////////////////////////////
//////***////    ����������    ////***///////
/////////////////////////////////////////////

// �����������
// delta    - ������ ������� ������ ������� �������
//            �����-�����
// iter_num - ����� �������� ������ ���������
// monte_carlo_points_num - ����� �����,
//            ������������ �������� ������ ������
template<int nPar, int nCrit> MontePawsAlgorithm<nPar, nCrit>::
                MontePawsAlgorithm(double delta, int iter_num,
                                   int monte_carlo_points_num)
    : _delta(delta), _iter_num(iter_num),
      _monte_carlo_points_num(monte_carlo_points_num)
{}

// ������ ���������
template<int nPar, int nCrit> void MontePawsAlgorithm<nPar, nCrit>::
                RunAlgorithm()
{
    // �������� ��������� �������
    if(_delta <= 0 || _iter_num < 1 ||
       _monte_carlo_points_num < 1  ||
       _criteria.empty())
        return;

    // ���� �� ���� ��������� ���������
    for(int r = 0; r < _iter_num; r++)
    {
        // ���������� ����������� �����;
        // �� ����� - ���������� ������
        FindCenterPoint();
        if(_centerPoint == nullptr)
            break;

        // ����� �������� �������������� ���������
        // ������� �����-����� ��� ����������, ����������
        // � ��������� ����������� �����
        void MinimizePersonalCrits();

        // ��������� ��������� ���������� �������� �����
        // ��������� �� ��������� ��������
        //**************************************************************
        // ���������� ��� �������������� ��� ����������� �����
        _centerPoint->satisfaction_bit = true;
        // ���������� ������� ����, ��� ����������� ����� �� �������
        _centerPoint = nullptr;
        //**************************************************************
    }
}
//-------------------------------------------------------------------

// ����� �������� �������������� ���������
// ������� �����-�����
template<int nPar, int nCrit> void MontePawsAlgorithm<nPar, nCrit>::
                MinimizePersonalCrits()
{
    // ������������� ��������� ������� �����
    // � �������� ����������
    _GenerateRandomPoints();

    // �������� ��������� �������
    if(_montecarlo_points.empty())
        return;
    //--------------------------------------------------------------

    // �������� ��������� ��������� �����
    _personal_mins.clear();
    //--------------------------------------------------------------

    // ���������� ����� �������������� ��������
    // �� ��������������� �����
    for(int crit_id = 0; crit_id < nCrit; crit_id++) {
        // ������ ������ �������� ��� ����������
        // � ������ ParetoPoint
        ParetoPoint<nPar, nCrit>::sort_crit_id = crit_id;
        // ����������� ��������� ����� �� ������ ������
        std::sort(_montecarlo_points.begin(), _montecarlo_points.end(),
                [](ParetoPointer &p1, ParetoPointer &p2) {
                     int sort_crit_id = ParetoPoint<nPar, nCrit>::sort_crit_id;
                     return p1->crits[sort_crit_id] < p2->crits[sort_crit_id];
                  }
        );
        _personal_mins.push_back(_montecarlo_points.front());
    }
    //--------------------------------------------------------------
}
//-------------------------------------------------------------------

// ������������� ��������� ����� ������ �������� ������;
// �� ��� ����� ����� �������� ������
template<int nPar, int nCrit> void MontePawsAlgorithm<nPar, nCrit>::
                _GenerateRandomPoints()
{
    // �������� ��������� ��������� �����
    _montecarlo_points.clear();
    //--------------------------------------------------------------

    // ������� ���� �� ��������� ��������� �����
    int point_id = 0;
    while(point_id < _monte_carlo_points_num) {
        // ������������� ��������� ��������� ����� � �������� ����������
        // � ������������ ����������: �������� ���� � ����� �������
        //**************************************************************
        // ������������� ����� �������
        double montecarlo_delta = (double)rand()/RAND_MAX * _delta;
        //--------------------------------------------------------------
        // ������� ������������� (nPar-1) ������������ ���������
        double cos_bunch[nPar];   // ����� ������������ ���������
        // ��������������� �������� ��� ����������� ���������
        // ��������� ������������ ���������
        double squared_interval = 1;
        for(int cos_id = 0; cos_id < (nPar-1); cos_id++)
        {
            // �������� ��������� ���������
            double half_interval = sqrt(squared_interval);
            // ������������� ������� � ��������� [-half_interval; half_interval]
            cos_bunch[cos_id] = -half_interval +
                    (double)rand()/RAND_MAX * (2*half_interval);

            // �������� ��������������� �������� ��� ���������
            // ���������� ��������
            squared_interval -= pow(cos_bunch[cos_id], 2);
        }
        //--------------------------------------------------------------
        // ������������� ��������� ������������ ������� ���,
        // ����� ����� ��������� ���� ��������� ���������� �������
        int sign = (rand()%2 > 0) ? (-1) : 1;   // ���� ������� ��������
        cos_bunch[nPar-1] = sign * sqrt(squared_interval);
        //**************************************************************

        // ������������ ������-�����: ������ � ��� ���������� �
        // ������ ������� �������
        double new_params[nPar];   // ��������� ������� ���������
        // �� �������� ���������
        for(int par_id = 0; par_id < nPar; par_id++) {
            // ������ ���������� ������
            new_params[par_id] = _centerPoint->params[par_id] +
                                 montecarlo_delta * cos_bunch[par_id];
        }
        ParetoPoint<nPar, nCrit> *new_point =
                ParetoPoint<nPar, nCrit>::FormPoint(new_params, _delta);
        // ���������, ������������� �� ����� ������������;
        // ���� ��� - ������ �� ���� (!)
        if(!new_point->ProveConstrains()) {
            point_id++;
            continue;
        }
        // ��������� �������� ��������� � ����� �����
        new_point->ComputeCriteria(_criteria);
        //--------------------------------------------------------------

        // �������� ����� �� �����
        ParetoPointer new_pareto_pointer(new_point);
        _montecarlo_points.push_back(new_pareto_pointer);

        point_id++;  // ������� � ��������� �����
    }  // ������� ���� �� ��������� ��������� �����
}
//-------------------------------------------------------------------

template<int nPar, int nCrit> void MontePawsAlgorithm<nPar, nCrit>::
                FindCenterPoint()
{
    // �������� ������������ � ������ ������ � ��������� �����,
    // ����� ������ ��������� ����������
    _archive_points.push_front(*(_archive_points.begin()));
    _archive_points.push_back (*(_archive_points.end()));

    // ������� �������� ����������
    // (-1) - ������� ����, ��� ������ ������� ����������� �����
    // ����������: ��� ������������ ����� ���������� ����� �������
    double maxNeighbourDist = -1;

    // ������ �� ���� ������ ������ � ������� ��������
    // ������������� � ����� ��������� ����������;
    // ������ � ��������� (�������������) ����� �� �������������
    for(auto p  = std::next(_archive_points.begin());
             p != std::prev(_archive_points.end());   p++)
    {        
        // ���� ����� ��� ���������� ����������� -
        // ���� � ���������
        if((*p)->satisfaction_bit)
            continue;

        // ��������� ������� ���������� �� ������� �
        // �������� � ��������� ����������
        double curNeighbourDist = _ComputeDistance(std::prev(p), *p) +
                                  _ComputeDistance(std::next(p), *p);
        if(curNeighbourDist > maxNeighbourDist) {
            maxNeighbourDist = curNeighbourDist;
            // std::next, ��������� ������ ������� �������������
            _centerPoint = (*p); }
    }

    // ������� ������������ ���������������� ������ � ��������� �����
    _archive_points.pop_front();  _archive_points.pop_back();
}
//-------------------------------------------------------------------

template<int nPar, int nCrit> double MontePawsAlgorithm<nPar, nCrit>::
                _ComputeDistance(const ParetoPointer &point1,
                                const ParetoPointer &point2) const
{
    double squaredSum = 0;
    for(int critId = 0; critId < nCrit; critId++)
        squaredSum += pow(point1->crits[critId] - point2->crits[critId], 2);
    return sqrt(squaredSum);
}

#endif
