TEMPLATE = app
CONFIG -= app_bundle
CONFIG -= console
CONFIG -= qt

QMAKE_CXXFLAGS += -std=c++11

SOURCES += \
    MontePawsCall.cpp

HEADERS += \
    MontePawsAlgorithm.h \
    ParetoPoint.h \
    Function.h

