#ifndef FUNCTION_H
#define FUNCTION_H

//! Abstract class of function
template<int nPar> class Function
{
public:
    //! Method computes fun value in point
    /*!
     * \param point is a double massive with nPar coords
     * \return a value of fun in a point
    */
    virtual double Compute(double* point) = 0;
};

#endif