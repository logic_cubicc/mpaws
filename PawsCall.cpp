////### FREE VARS ###/////////////
#define NUM_CRIT   2
#define NUM_PAR    2
// select which function to appr
// #define PARAB		// uncomment ZDT3 too!
// #define conv
// #define nonconv
#define ZDT3
// #define ZDT6
// #define ZDT7
// #define DIBAG
// #define EXPER_CONS	// enables experimental cons on
			// variables
// #define EXPER_START_PT  // enables experimental start point
#define ESTIMATION
// #define RESTORE		// restore pts from file
#ifdef  RESTORE
#define REST_ITER    900
#endif
#define COLOR_PTS
// GNUPLOT PARAMS
#define NONDOM_RAD   0.03
#define OTHER_RAD    0.02
#define SNONDOM_RAD  0.03/3
#define SOTHER_RAD   0.02/3
/////////////////////////////////
//!!! not good: C libraries for I/O
#include <cstdio>
#include <cstring>
#define BUFSIZE 512

#include <fstream>
#include <iostream>

#include "AlgorithmNelderaMida.h"
#include "Approximator.h"
#include "Approximator2.h"
#include "ApproximatorCCD.h"
#include "CCDesign.h"
#include "CompositeFunction.h"
#include "DibagMXSE.h"
#include "DibagMLSE.h"
#include "HyperSphereFunction.h"
#include "InequalityFunctional.h"
#include "LeftOneParameterConstrain.h"
#include "LinearConstrain.h"
#include "MSU_NMAlg.h"
#include "ParabaloidFunction.h"
#include "ParetoMeasurer.h"
#include "ParetoPoint.h"
#include "PawsAlgorithm.h"
#include "QuickSort.h"
#include "RightOneParameterConstrain.h"
#include "Tester.h"
#include "MSU_SlaceTolerAlg.h"

#include "zdt3_f1.h"
#include "zdt3_f2.h"

#include "zdt6_f1.h"
#include "zdt6_f2.h"

#include "zdt7_f1.h"
#include "zdt7_f2.h"

#include "conv_f1.h"
#include "conv_f2.h"
#include "nonconv_f1.h"
#include "nonconv_f2.h"

#include "gnuplot_i.hpp"
#include <vector>

// define numbers of functions runs
// (static variables)
template<> int Conv_f1<NUM_PAR>::run_counter = 0;
template<> int Conv_f2<NUM_PAR>::run_counter = 0;
template<> int NonConv_f1<NUM_PAR>::run_counter = 0;
template<> int NonConv_f2<NUM_PAR>::run_counter = 0;

template<> int ZDT3_f1<NUM_PAR>::run_counter = 0;
template<> int ZDT3_f2<NUM_PAR>::run_counter = 0;

template<> int ZDT6_f1<NUM_PAR>::run_counter = 0;
template<> int ZDT6_f2<NUM_PAR>::run_counter = 0;

template<> int ZDT7_f1<NUM_PAR>::run_counter = 0;
template<> int ZDT7_f2<NUM_PAR>::run_counter = 0;

template<> int DibagMXSE<NUM_PAR>::run_counter = 0;
template<> int DibagMLSE<NUM_PAR>::run_counter = 0;

template<> int ApproximatorCCD<NUM_PAR>::debug_flag = 0;

int main(int argc, char** argv)
{
    srand((unsigned)time(NULL)); // global random seed gen
    double delta = atof(argv[2]);
    double ro = atof(argv[4]);
    double tau = atof(argv[6]);
    
    // NOTE:  Paws algorithm
    PawsAlgorithm<NUM_PAR, NUM_CRIT>* paws_algorithm = new PawsAlgorithm<NUM_PAR, NUM_CRIT>(delta, ro, tau);
    
    // NOTE: Parabaloid function
    double* fractions = new double[NUM_PAR];    
    for(int i = 0; i < NUM_PAR; ++i)
	fractions[i] = 1;
    double* first_offsets = new double[NUM_PAR]; 
    for(int i = 0; i < NUM_PAR; ++i)
	first_offsets[i] = 0;
    double* second_offsets = new double[NUM_PAR]; 
    for(int i = 0; i < NUM_PAR; ++i)
	second_offsets[i] = 1;
    
    ParabaloidFunction<NUM_PAR>* first_parab_function = new ParabaloidFunction<NUM_PAR>(fractions, first_offsets);
    ParabaloidFunction<NUM_PAR>* second_parab_function = new ParabaloidFunction<NUM_PAR>(fractions, second_offsets);
    //END OF Parabaloid function
    
    // NOTE: conv
    #ifdef conv
    Conv_f1<NUM_PAR>* f1 = new Conv_f1<NUM_PAR>;
    Conv_f2<NUM_PAR>* f2 = new Conv_f2<NUM_PAR>;
    #endif
    // NOTE: nonconv
    #ifdef nonconv
    NonConv_f1<NUM_PAR>* f1 = new NonConv_f1<NUM_PAR>;
    NonConv_f2<NUM_PAR>* f2 = new NonConv_f2<NUM_PAR>;
    #endif
    // NOTE: zdt3
    #ifdef ZDT3
    ZDT3_f1<NUM_PAR>* f1 = new ZDT3_f1<NUM_PAR>;
    ZDT3_f2<NUM_PAR>* f2 = new ZDT3_f2<NUM_PAR>;
    #endif
    // NOTE: zdt6
    #ifdef ZDT6
    ZDT6_f1<NUM_PAR>* f1 = new ZDT6_f1<NUM_PAR>;
    ZDT6_f2<NUM_PAR>* f2 = new ZDT6_f2<NUM_PAR>;
    #endif
    // NOTE: zdt7
    #ifdef ZDT7
    ZDT7_f1<NUM_PAR>* f1 = new ZDT7_f1<NUM_PAR>;
    ZDT7_f2<NUM_PAR>* f2 = new ZDT7_f2<NUM_PAR>;
    #endif
    // NOTE: dibag
    #ifdef DIBAG
    DibagMXSE<NUM_PAR>* f1 = new DibagMXSE<NUM_PAR>;
    DibagMLSE<NUM_PAR>* f2 = new DibagMLSE<NUM_PAR>;
    #endif
 
    // NOTE: Add functions to Paws
    #ifndef PARAB
    paws_algorithm->AddFunction(f1);
    paws_algorithm->AddFunction(f2);
    #else
    paws_algorithm->AddFunction(first_parab_function);
    paws_algorithm->AddFunction(second_parab_function);
    #endif
    //END OF Addition function

    // NOTE: Add constrains to Paws
    #ifdef DIBAG
    for(int par_id = 0; par_id < NUM_PAR; ++par_id)
    {
	LeftOneParameterConstrain<NUM_PAR>* left_wall_cons = new LeftOneParameterConstrain<NUM_PAR>(0.0, par_id);
	RightOneParameterConstrain<NUM_PAR>* right_wall_cons = new RightOneParameterConstrain<NUM_PAR>(1.0, par_id);
	paws_algorithm->AddConstrain(left_wall_cons);
	paws_algorithm->AddConstrain(right_wall_cons);
    }
    
    for(int exper_id = 0; exper_id < 3; ++exper_id)
    {
	{
	double cons_fr[NUM_PAR];
	for(int i = 0; i < NUM_PAR; ++i)
	    cons_fr[i] = 0.0;
	cons_fr[4*exper_id + 3] = 1.0;
	cons_fr[4*exper_id + 1] = -1.0;
	double constant = 0.0;
	LinearConstrain<NUM_PAR>* linear_cons = new LinearConstrain<NUM_PAR>(cons_fr, constant);
	paws_algorithm->AddConstrain(linear_cons);
	}
	{
	double cons_fr[NUM_PAR];
	for(int i = 0; i < NUM_PAR; ++i)
	    cons_fr[i] = 0.0;
	cons_fr[4*exper_id + 1] = 1.0;
	cons_fr[4*exper_id + 0] = -1.0;
	double constant = 0.0;
	LinearConstrain<NUM_PAR>* linear_cons = new LinearConstrain<NUM_PAR>(cons_fr, constant);
	paws_algorithm->AddConstrain(linear_cons);
	}
    }
    // k raises with T
    for(int fr_id = 0; fr_id < 4; ++fr_id)
    {
	{
	double cons_fr[NUM_PAR];
	for(int i = 0; i < NUM_PAR; ++i)
	    cons_fr[i] = 0.0;
	cons_fr[fr_id] = -1.0;
	cons_fr[fr_id + 4] = 1.0;
	double constant = 0.0;
	LinearConstrain<NUM_PAR>* linear_cons = new LinearConstrain<NUM_PAR>(cons_fr, constant);
	paws_algorithm->AddConstrain(linear_cons);
	}
	{
	double cons_fr[NUM_PAR];
	for(int i = 0; i < NUM_PAR; ++i)
	    cons_fr[i] = 0.0;
	cons_fr[fr_id + 4] = -1.0;
	cons_fr[fr_id + 8] = 1.0;
	double constant = 0.0;
	LinearConstrain<NUM_PAR>* linear_cons = new LinearConstrain<NUM_PAR>(cons_fr, constant);
	paws_algorithm->AddConstrain(linear_cons);
	}
    }
    
    #else
    for(int i = 0; i < NUM_PAR; ++i)
    {
	RightOneParameterConstrain<NUM_PAR>* right_wall_cons = new RightOneParameterConstrain<NUM_PAR>(1, i);
	LeftOneParameterConstrain<NUM_PAR>* left_wall_cons = new LeftOneParameterConstrain<NUM_PAR>(0, i);
    
	paws_algorithm->AddConstrain(right_wall_cons);
	paws_algorithm->AddConstrain(left_wall_cons);
    }
    #endif
    /*double cons_fr[NUM_PAR];
    for(int i = 0; i < NUM_PAR; ++i)
	cons_fr[i] = -1.0;
    double constant = 1.0;
    LinearConstrain<NUM_PAR>* linear_cons = new LinearConstrain<NUM_PAR>(cons_fr, constant);
    paws_algorithm->AddConstrain(linear_cons);*/
    //END OF Add constrains to Paws
    
    // NOTE: Central Composite Design
    CCDesign<NUM_PAR, NUM_CRIT>* cc_design = new CCDesign<NUM_PAR, NUM_CRIT>;
    cc_design->SetPawsAlgorithm(paws_algorithm);
    
    paws_algorithm->SetDesign(cc_design);
    //END OF Central Composite Design
    
    // NOTE: SortClass
    QuickSort<NUM_PAR, NUM_CRIT>* quick_sort = new QuickSort<NUM_PAR, NUM_CRIT>;
    quick_sort->SetPawsAlgorithm(paws_algorithm);
    
    paws_algorithm->SetSortClass(quick_sort);
    //END OF SortClass
    
    #ifndef RESTORE
    // NOTE: Define first point
    double* params = new double[NUM_PAR]; 
    #ifdef DIBAG
    params[0] = 0.3;
    params[1] = 0.5;
    params[2] = 0.5;
    params[3] = 0.7;
    
    params[4] = 0.4;
    params[5] = 0.6;
    params[6] = 0.6;
    params[7] = 0.8;
    
    params[8]  = 0.5;
    params[9]  = 0.7;
    params[10] = 0.7;
    params[11] = 0.9;
    #else
    for(int i = 0; i < NUM_PAR; ++i)
	/*params[i] = (double)rand()/RAND_MAX;*/ params[i] = 0.5;
//     params[0] = 0.2; params[1] = 0.2;
    #endif
    ParetoPoint<NUM_PAR, NUM_CRIT>* par_point = ParetoPoint<NUM_PAR, NUM_CRIT>::FormPoint(params, 0);
    paws_algorithm->ComputeFunctions(par_point);
    paws_algorithm->AddPoint(par_point);
    //END OF Define first point
    #else
    //! restore pts from file
    paws_algorithm->RestorePoints(REST_ITER); // last made iteration (in "human" notation)
				       // "i want to restore this iterations num"
    #endif
    
    //! Running algorithm
    //paws_algorithm->RunAlgorithm(atoi(argv[8])); 
    //END OF Running algorithm
    
    //TEST approximation test
    double nm_accuracy = 0.001;
    double nm_itmax    = 1000;
    double nm_maxk     = 1000;
    double nm_edge     = 1;
    
    double nm_start_pt[NUM_PAR];
    for(int par_id = 0; par_id < NUM_PAR; ++par_id)
	nm_start_pt[par_id] = 0;
       
    MSU_NMAlg<NUM_PAR> msu_nm(nm_accuracy, nm_itmax, nm_maxk,
			      nm_edge,	nm_start_pt);
    // set parabaloid fun 
    // with center offset = 1 
    msu_nm.SetFunction(second_parab_function);
    msu_nm.RunAlgorithm();
    //END OF approximation test
    
    //! output number of pts & iterations done
    std::cerr << std::endl;
    std::cerr << "iterations made: " << paws_algorithm->r << std::endl;
    std::cerr << "pareto points: " << paws_algorithm->GetNumberOfPoints() << std::endl;
    std::cerr << std::endl;
    
    //! output number of functions runs
    #ifdef ESTIMATION
    #ifdef conv
    std::cerr << "conv_f1 runs: " << Conv_f1<NUM_PAR>::run_counter << std::endl;
    std::cerr << "conv_f2 runs: " << Conv_f2<NUM_PAR>::run_counter << std::endl;
    std::cerr << std::endl;
    #endif 
    #ifdef nonconv
    std::cerr << "nonconv_f1 runs: " << NonConv_f1<NUM_PAR>::run_counter << std::endl;
    std::cerr << "nonconv_f2 runs: " << NonConv_f2<NUM_PAR>::run_counter << std::endl;
    std::cerr << std::endl;
    #endif 
    #ifdef ZDT3
    std::cerr << "ZDT3_f1 runs: " << ZDT3_f1<NUM_PAR>::run_counter << std::endl;
    std::cerr << "ZDT3_f2 runs: " << ZDT3_f2<NUM_PAR>::run_counter << std::endl;
    std::cerr << std::endl;
    #endif   
    #ifdef ZDT6
    std::cerr << "ZDT6_f1 runs: " << ZDT6_f1<NUM_PAR>::run_counter << std::endl;
    std::cerr << "ZDT6_f2 runs: " << ZDT6_f2<NUM_PAR>::run_counter << std::endl;
    std::cerr << std::endl;
    #endif    
    #ifdef ZDT7
    std::cerr << "ZDT7_f1 runs: " << ZDT7_f1<NUM_PAR>::run_counter << std::endl;
    std::cerr << "ZDT7_f2 runs: " << ZDT7_f2<NUM_PAR>::run_counter << std::endl;
    std::cerr << std::endl;
    #endif
    #endif
    
    // Visualizing with gnuplot
    std::vector<double> x, y, sx, sy;
    int non_dom_pts_num = paws_algorithm->GetNumberOfPoints();
    for(int i = 0; i < non_dom_pts_num; ++i)
    {
	ParetoPoint<NUM_PAR, NUM_CRIT>* par_point = paws_algorithm->GetPoint(i);
	x.push_back(par_point->crits[0]);
	y.push_back(par_point->crits[1]);
	
	sx.push_back(par_point->params[0]);
	sy.push_back(par_point->params[1]);
    }
    
    std::vector<double> arch_centers_x,  arch_centers_y,
			sarch_centers_x, sarch_centers_y;
    int arch_centers_pts_num = paws_algorithm->arch_centers.size();
    for(int i = 0; i < arch_centers_pts_num; ++i)
    {
	ParetoPoint<NUM_PAR, NUM_CRIT>* par_point = paws_algorithm->arch_centers[i];
	arch_centers_x.push_back(par_point->crits[0]);
	arch_centers_y.push_back(par_point->crits[1]);
	
	sarch_centers_x.push_back(par_point->params[0]);
	sarch_centers_y.push_back(par_point->params[1]);
    }
    
    std::vector<double> arch_des_pts_x,  arch_des_pts_y,
			sarch_des_pts_x, sarch_des_pts_y;
    int arch_des_pts_num = paws_algorithm->arch_des_pts.size();
    for(int i = 0; i < arch_des_pts_num; ++i)
    {
	ParetoPoint<NUM_PAR, NUM_CRIT>* par_point = paws_algorithm->arch_des_pts[i];
	arch_des_pts_x.push_back(par_point->crits[0]);
	arch_des_pts_y.push_back(par_point->crits[1]);
	
	sarch_des_pts_x.push_back(par_point->params[0]);
	sarch_des_pts_y.push_back(par_point->params[1]);
    }
    
    std::vector<double> arch_opt_single_sols_x,  arch_opt_single_sols_y,
			sarch_opt_single_sols_x, sarch_opt_single_sols_y;
    int arch_opt_single_sols_pts_num = paws_algorithm->arch_opt_single_sols.size();
    for(int i = 0; i < arch_opt_single_sols_pts_num; ++i)
    {
	ParetoPoint<NUM_PAR, NUM_CRIT>* par_point = paws_algorithm->arch_opt_single_sols[i];
	arch_opt_single_sols_x.push_back(par_point->crits[0]);
	arch_opt_single_sols_y.push_back(par_point->crits[1]);
	
	sarch_opt_single_sols_x.push_back(par_point->params[0]);
	sarch_opt_single_sols_y.push_back(par_point->params[1]);
    }
    
    std::vector<double> arch_opt_complex_sols_x,  arch_opt_complex_sols_y,
			sarch_opt_complex_sols_x, sarch_opt_complex_sols_y;
    int arch_opt_complex_sols_pts_num = paws_algorithm->arch_opt_complex_sols.size();
    for(int i = 0; i < arch_opt_complex_sols_pts_num; ++i)
    {
	ParetoPoint<NUM_PAR, NUM_CRIT>* par_point = paws_algorithm->arch_opt_complex_sols[i];
	arch_opt_complex_sols_x.push_back(par_point->crits[0]);
	arch_opt_complex_sols_y.push_back(par_point->crits[1]);
	
	sarch_opt_complex_sols_x.push_back(par_point->params[0]);
	sarch_opt_complex_sols_y.push_back(par_point->params[1]);
    }
    
    //! form output
    std::ofstream fout ("vis_out.gp");
    std::ofstream fout0("nondom_pts.txt");
    std::ofstream fout1("arch_centers.txt");
    std::ofstream fout2("arch_des_pts.txt");
    std::ofstream fout3("arch_opt_single_sols.txt");
    std::ofstream fout4("arch_opt_complex_sols.txt");
    std::ofstream fout5("vis_out_short.gp");
    
    std::ofstream sfout ("svis_out.gp");
    std::ofstream sfout0("snondom_pts.txt");
    std::ofstream sfout1("sarch_centers.txt");
    std::ofstream sfout2("sarch_des_pts.txt");
    std::ofstream sfout3("sarch_opt_single_sols.txt");
    std::ofstream sfout4("sarch_opt_complex_sols.txt");
    std::ofstream sfout5("svis_out_short.gp");
    
    if( !fout  || !fout0  || !fout1  || !fout2  || !fout3  || !fout4  || !fout5 ||
	!sfout || !sfout0 || !sfout1 || !sfout2 || !sfout3 || !sfout4 || !sfout5)
    {
	std::cerr << "Can't open vis file: " << std::endl;
	return -1;
    }
    //! nondom pts
    for(int i = 0; i < non_dom_pts_num; ++i)
	fout0 << x[i] << " " << y[i] << " " << NONDOM_RAD << std::endl;
    //! arch_centers
    for(int i = 0; i < arch_centers_pts_num; ++i)
	fout1 << arch_centers_x[i] << " " << arch_centers_y[i] << " " << OTHER_RAD << std::endl;
    //! arch_des_pts
    for(int i = 0; i < arch_des_pts_num; ++i)
	fout2 << arch_des_pts_x[i] << " " << arch_des_pts_y[i] << " " << OTHER_RAD << std::endl;
    //! arch_opt_single_sols
    for(int i = 0; i < arch_opt_single_sols_pts_num; ++i)
	fout3 << arch_opt_single_sols_x[i] << " " << arch_opt_single_sols_y[i] << " " << OTHER_RAD << std::endl;
    //! arch_opt_complex_sols
    for(int i = 0; i < arch_opt_complex_sols_pts_num; ++i)
	fout4 << arch_opt_complex_sols_x[i] << " " << arch_opt_complex_sols_y[i] << " " << OTHER_RAD << std::endl;
    
    //! snondom pts
    for(int i = 0; i < non_dom_pts_num; ++i)
	sfout0 << sx[i] << " " << sy[i] << " " << SNONDOM_RAD << std::endl;
    //! sarch_centers
    for(int i = 0; i < arch_centers_pts_num; ++i)
	sfout1 << sarch_centers_x[i] << " " << sarch_centers_y[i] << " " << SOTHER_RAD << std::endl;
    //! sarch_des_pts
    for(int i = 0; i < arch_des_pts_num; ++i)
	sfout2 << sarch_des_pts_x[i] << " " << sarch_des_pts_y[i] << " " << SOTHER_RAD << std::endl;
    //! sarch_opt_single_sols
    for(int i = 0; i < arch_opt_single_sols_pts_num; ++i)
	sfout3 << sarch_opt_single_sols_x[i] << " " << sarch_opt_single_sols_y[i] << " " << SOTHER_RAD << std::endl;
    //! sarch_opt_complex_sols
    for(int i = 0; i < arch_opt_complex_sols_pts_num; ++i)
	sfout4 << sarch_opt_complex_sols_x[i] << " " << sarch_opt_complex_sols_y[i] << " " << SOTHER_RAD << std::endl;
    
    //! gp comm file
    fout << "set grid" << std::endl;
    fout << "set xtics 0.5" << std::endl;
    fout << "set ytics 0.5" << std::endl;
    fout << "set nokey" << std::endl;
    fout << "set xrange [0:4]" << std::endl;
    fout << "set yrange [0:4]" << std::endl;
    fout << "set size square" << std::endl;
    fout << std::endl;
    //! gp short comm file
    fout5 << "set grid" << std::endl;
    fout5 << "set xtics 0.5" << std::endl;
    fout5 << "set ytics 0.5" << std::endl;
    fout5 << "set nokey" << std::endl;
    fout5 << "set xrange [0:4]" << std::endl;
    fout5 << "set yrange [0:4]" << std::endl;
    fout5 << "set size square" << std::endl;
    fout5 << std::endl;
    
    //! sgp comm file
    sfout << "set grid" << std::endl;
    sfout << "set xtics 0.2" << std::endl;
    sfout << "set ytics 0.2" << std::endl;
    sfout << "set nokey" << std::endl;
    sfout << "set xrange [0:1]" << std::endl;
    sfout << "set yrange [0:1]" << std::endl;
    sfout << "set size square" << std::endl;
    sfout << std::endl;
    //! sgp short comm file
    sfout5 << "set grid" << std::endl;
    sfout5 << "set xtics 0.2" << std::endl;
    sfout5 << "set ytics 0.2" << std::endl;
    sfout5 << "set nokey" << std::endl;
    sfout5 << "set xrange [0:1]" << std::endl;
    sfout5 << "set yrange [0:1]" << std::endl;
    sfout5 << "set size square" << std::endl;
    sfout5 << std::endl;

    fout  << "set multiplot" << std::endl;
    sfout << "set multiplot" << std::endl;
    #ifdef COLOR_PTS
    fout << "p 'arch_des_pts.txt' w circles lc rgb \"#1CB5BD\"" << std::endl;  	       // blue
    fout << "p 'arch_opt_single_sols.txt' w circles lc rgb \"#FF004C\"" << std::endl;  // purple
    fout << "p 'arch_opt_complex_sols.txt' w circles lc rgb \"#FCFC2B\"" << std::endl; // yellow
    fout << "p 'arch_centers.txt' w circles lc rgb \"green\"" << std::endl;  	       // brown
    fout << "set style fill solid" << std::endl;
    fout << "p 'nondom_pts.txt' w circles" << std::endl;
    
    fout5 << "set style fill solid" << std::endl;
    fout5 << "p 'nondom_pts.txt' w circles" << std::endl;
    
    sfout << "p 'sarch_des_pts.txt' w circles lc rgb \"#1CB5BD\"" << std::endl;  	       // blue
    sfout << "p 'sarch_opt_single_sols.txt' w circles lc rgb \"#FF004C\"" << std::endl;  // purple
    sfout << "p 'sarch_opt_complex_sols.txt' w circles lc rgb \"#FCFC2B\"" << std::endl; // yellow
    sfout << "p 'sarch_centers.txt' w circles lc rgb \"green\"" << std::endl;  	       // brown
    sfout << "set style fill solid" << std::endl;
    sfout << "p 'snondom_pts.txt' w circles" << std::endl;
    
    sfout5 << "set style fill solid" << std::endl;
    sfout5 << "p 'snondom_pts.txt' w circles" << std::endl;
    
    #else
    fout << "p 'arch_des_pts.txt' w circles lw 0.3 lt -1" << std::endl;
    fout << "p 'arch_opt_single_sols.txt' w circles lw 0.3 lt -1" << std::endl;
    fout << "p 'arch_opt_complex_sols.txt' w circles lw 0.3 lt -1" << std::endl;
    fout << "p 'arch_centers.txt' w circles lw 0.3 lt -1" << std::endl;
    fout << "set style fill solid" << std::endl;
    fout << "p 'nondom_pts.txt' w circles lw 0.3 lt -1" << std::endl;
    
    fout5 << "set style fill solid" << std::endl;
    fout5 << "p 'nondom_pts.txt' w circles lw 0.3 lt -1" << std::endl;
    
    sfout << "p 'sarch_des_pts.txt' w circles lw 0.3 lt -1" << std::endl;
    sfout << "p 'sarch_opt_single_sols.txt' w circles lw 0.3 lt -1" << std::endl;
    sfout << "p 'sarch_opt_complex_sols.txt' w circles lw 0.3 lt -1" << std::endl;
    sfout << "p 'sarch_centers.txt' w circles lw 0.3 lt -1" << std::endl;
    sfout << "set style fill solid" << std::endl;
    sfout << "p 'snondom_pts.txt' w circles lw 0.3 lt -1" << std::endl;
    
    sfout5 << "set style fill solid" << std::endl;
    sfout5 << "p 'snondom_pts.txt' w circles lw 0.3 lt -1" << std::endl;
    #endif
    fout  << "set nomultiplot" << std::endl;
    fout  << std::endl;
    sfout << "set nomultiplot" << std::endl;
    sfout << std::endl;
    
    fout   << "pause -1" << std::endl;
    fout   << std::endl;
    fout5  << "pause -1" << std::endl;
    fout5  << std::endl;
    sfout  << "pause -1" << std::endl;
    sfout  << std::endl;
    sfout5 << "pause -1" << std::endl;
    sfout5 << std::endl;
    //END OF form output
    
    #ifdef ESTIMATION
    // read points of precise Pareto Front
    // TODO: not good!!!
    #ifdef conv
    FILE* fd = fopen("conv.txt", "r");
    #endif
    #ifdef nonconv
    FILE* fd = fopen("nonconv.txt", "r");
    #endif
    #ifdef ZDT3
    FILE* fd = fopen("ZDT3.txt", "r");
    #endif
    #ifdef ZDT6
    FILE* fd = fopen("ZDT6.txt", "r");
    #endif
    #ifdef ZDT7
    FILE* fd = fopen("ZDT7.txt", "r");
    #endif
    
    // vectors to store precise Pareto Front
    std::vector<double> x2, y2;
    // parse file
    char buf[BUFSIZE];
    while (fgets(buf, BUFSIZE, fd) != NULL)
    {
        char* val = strtok(buf, " ");
	x2.push_back(atof(val));
	
	val = strtok(NULL, " ");
	y2.push_back(atof(val));
    }
    
    // ParetoMeasurer
    ParetoMeasurer<NUM_CRIT> pareto_measurer(x, y, x2, y2);
    double GP = pareto_measurer.ComputeGenerationalDistance();
    double SP = pareto_measurer.ComputeSpacing();
    std::cerr << "generational distance: " << GP << std::endl;
    std::cerr << "spacing: " 		   << SP << std::endl;
    std::cerr << std::endl;
    fclose(fd);
    #endif
    return 0;
}
