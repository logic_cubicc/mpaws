#ifndef PAWSALGORITHM_H
#define PAWSALGORITHM_H

////### FREE VARS ###///////////////
#define APPR                      // approximation is turned on
#define CCD                       // approximation with CCD
                                  // alternitavely - witn Radial Neural Net
#define BRUT_HELP                 // a little help as initial pareto
                                  //                        brutforcing
// #define BRUT_STEP        0.25  // mesh step
#define BRUT_PTS_NUM         100  // mesh step
#define ADD_DESIGN_POINTS         // has theoretical needs
// #define END_PTS_OPT            // solve or not to solve opt problems in end pts
// #define TR_MOVING              // enables moving trust region
// #define VERBOSE                // NOTE: verbose mode of algorithm
// #define OPT_DEBUG              // local optimization verbose mode
#define OUTPUT_LIMIT          10  // 
#define OVERLAP_THRESHOLD  0.0001  // max interspace of pts in archive
#define MSU_COND_OPTIM            // use MSU algorithms
#define ACCURACY           0.001  // Accuracy for single-optimization problem
// MSU     Algorithm params
#define ITMAX               2000  // max num iters for f(x) opt
#define ITMT               10000  // max num iters for T(x) opt
#define MAXK               20000  // max num of f(x) estims
#define MAXT               50000  // max num of T(x) estims
// Penalty Algorithm params
#define ALPHA              10000  // Fraction for Penalty Algorithm
#define CONS_ACCURACY     -0.001  // Constration accuracy for Penalty Algorithm
////////////////////////////////////

#include "ApproximatorCCD.h"
#include "Approximator2.h"
#include "Design.h"
#include "DibagMXSE.h"
#include "DibagMLSE.h"
#include "Function.h"
#include "HyperSphereFunction.h"
#include "MSU_SlaceTolerAlg.h"
#include "NormFunction.h"
#include "ParetoBrootForcer.h"
#include "ParetoPoint.h"
#include "PenaltyAlgorithm.h"

template<int , int > class QuickSort;

#include <fstream>
#include <sstream>
#include <ostream>
#include <iomanip>
#include <cstring>

#include <algorithm>
#include <cmath>
#include <iostream>
#include <list>
#include <stdlib.h>
#include <time.h>
#include <vector>

template<> int MSU_SlaceTolerAlg<NUM_PAR>::iter_num = 0;

template<int nPar, int nCrit> class PawsAlgorithm
{
public:    
    PawsAlgorithm(double delta, double ro, double tau);
    
    void SetDesign(Design<nPar, nCrit>* );
    void SetSortClass(QuickSort<nPar, nCrit>* );
    
    void AddConstrain(Function<nPar>* );
    void AddFunction(Function<nPar>* );
    
    void AddPoint(ParetoPoint<nPar, nCrit>* );
    void AddPoint(ParetoPoint<nPar, nCrit>* par_point, 
                  std::vector<ParetoPoint<nPar, nCrit>*>& cont);
    
    ParetoPoint<nPar, nCrit>* GetPoint(int );
    int GetNotSatisfiedPoint(int );
    int GetNumberOfPoints();
    int GetNumberOfSatisfiedPoints();
    void PrintPoints();
    
    void ComputeFunctions(ParetoPoint<nPar, nCrit>* );
    void ComputeApprFunctions(ParetoPoint<nPar, nCrit>* );
    
    int ProveConstrains(ParetoPoint<nPar, nCrit>* );
    
    void RunAlgorithm(int iter_num);
    int CenterPointDetermination();

    int IsPointListed(ParetoPoint<nPar, nCrit>* );
    
    int FindCenterPoint();
    double FindDistance(int );
    void SetDesignPoints(int , double );
    void SortPoints();
    void DeleteOverlapSolutions();
    void FindNonDominatedPoints(typename std::list<ParetoPoint<nPar, nCrit>*>::iterator,
                                std::list<ParetoPoint<nPar, nCrit>*>& cont);
    
    void SetDesignPoints(std::vector<std::vector<std::pair<std::vector<double> ,double > > >& global_design_points,
                                                                            double delta);
    bool FitTrustRegion(double new_rad, ParetoPoint<nPar, nCrit>*& tr_reg);
    void CleanPoints();
    void SavePoints();
    void RestorePoints(int r);

// private:
    std::vector<Function<nPar>*> constrains;
    std::vector<Function<nPar>*> functions;
    std::vector<Function<nPar>*> appr_funs;
    
    Design<nPar, nCrit>* design;
    QuickSort<nPar, nCrit>* sort_class;
    
    std::list<ParetoPoint<nPar, nCrit>*> points;
    //! bunch of archive sets
    std::vector<ParetoPoint<nPar, nCrit>*> arch_des_pts; 
    std::vector<ParetoPoint<nPar, nCrit>*> arch_centers; 
    std::vector<ParetoPoint<nPar, nCrit>*> arch_opt_single_sols;
    std::vector<ParetoPoint<nPar, nCrit>*> arch_opt_complex_sols;
    
    double delta;
    double ro;
    double tau;
    
    //! num of iters
    int r, real_r;
};

template<int nPar, int nCrit> PawsAlgorithm<nPar, nCrit>::
                PawsAlgorithm(double delta, double ro, double tau)
{
    appr_funs.resize(nCrit);
    r = 0;
    real_r = 0;
    
    this->delta = delta;
    this->ro = ro;
    this->tau = tau;
}

template<int nPar, int nCrit> void PawsAlgorithm<nPar, nCrit>::
                AddConstrain(Function<nPar>* constrain)
{
    constrains.push_back(constrain);
}

template<int nPar, int nCrit> void PawsAlgorithm<nPar, nCrit>::
                AddFunction(Function<nPar>* function)
{
    functions.push_back(function);
}

template<int nPar, int nCrit> void PawsAlgorithm<nPar, nCrit>::
                AddPoint(ParetoPoint<nPar, nCrit>* par_point)
{
    points.push_back(par_point);
}


template<int nPar, int nCrit> void PawsAlgorithm<nPar, nCrit>::
                AddPoint(ParetoPoint<nPar, nCrit>* par_point, 
                         std::vector<ParetoPoint<nPar, nCrit>*>& cont)
{
    cont.push_back(par_point);
}

template<int nPar, int nCrit> void PawsAlgorithm<nPar, nCrit>::
                ComputeFunctions(ParetoPoint<nPar, nCrit>* par_point)
{
    #ifdef DIBAH
    DibahSSE<nPar>* fun0 = static_cast<DibahSSE<nPar>*>(functions[0]);
    fun0->Compute(par_point->params);
    par_point->crits[0] = fun0->sse;
    par_point->crits[1] = fun0->max_der_te;
    #else
    for(int i = 0; i < functions.size(); ++i)
        par_point->crits[i] = functions[i]->Compute(par_point->params);
    #endif
}

template<int nPar, int nCrit> void PawsAlgorithm<nPar, nCrit>::
                ComputeApprFunctions(ParetoPoint<nPar, nCrit>* par_point)
{
    for(int i = 0; i < functions.size(); ++i)
        par_point->crits[i] = appr_funs[i]->Compute(par_point->params);
}

template<int nPar, int nCrit> int PawsAlgorithm<nPar, nCrit>::
                ProveConstrains(ParetoPoint<nPar, nCrit>* par_point)
{
    for(int i = 0; i < constrains.size(); ++i)
        if(constrains[i]->Compute(par_point->params) < 0)
            return 1;
    //good
    return 0;
}

template<int nPar, int nCrit> void PawsAlgorithm<nPar, nCrit>::
                SetDesign(Design<nPar, nCrit>* in_design)
{
    design = in_design;
}

template<int nPar, int nCrit> void PawsAlgorithm<nPar, nCrit>::
                SetSortClass(QuickSort<nPar, nCrit>* in_sort_class)
{
    sort_class = in_sort_class;
}

template<int nPar, int nCrit> void PawsAlgorithm<nPar, nCrit>::
                SetDesignPoints(int center_num, double delta)
{
    design->SetDesignPoints(GetPoint(center_num), delta);
}

template<int nPar, int nCrit> void PawsAlgorithm<nPar, nCrit>::
                SetDesignPoints(std::vector<std::vector<std::pair<std::vector<double> ,double > > >& global_design_points,
                                                        double delta)
{
    std::vector<std::pair<std::vector<double> ,double > > first_design_points = global_design_points[0];
    for(int design_point_id = 0; design_point_id < first_design_points.size(); ++design_point_id)
    {
        // current design point pair
        std::pair<std::vector<double> ,double > design_point_pair = first_design_points[design_point_id];
        
        double params[nPar];
        for(int par_id = 0; par_id < nPar; ++par_id)
            params[par_id] = design_point_pair.first[par_id];
        double crits[nCrit];
        for(int crit_id = 0; crit_id < nCrit; ++crit_id)
            crits[crit_id] = (global_design_points[crit_id][design_point_id]).second;
        
        ParetoPoint<nPar, nCrit>* par_point = ParetoPoint<nPar, nCrit>::FormPoint(params, crits, 0, false);
        if(IsPointListed(par_point))
            AddPoint(par_point);
        //! copy design points to archive of design points
        AddPoint(par_point, arch_des_pts);
    }
}

template<int nPar, int nCrit> void PawsAlgorithm<nPar, nCrit>::
                SortPoints()
{
    sort_class->Sort(0, GetNumberOfPoints() - 1);
}

template<int nPar, int nCrit> ParetoPoint<nPar, nCrit>* PawsAlgorithm<nPar, nCrit>::
                GetPoint(int point_num)
{
    typename std::list<ParetoPoint<nPar, nCrit>*>::iterator p;
    
    int i = 0;
    for(p = points.begin(); i < point_num; ++p, ++i);
    
    return *p;
}

template<int nPar, int nCrit> int PawsAlgorithm<nPar, nCrit>::
                GetNotSatisfiedPoint(int not_sat_point_id)
{
    int id = -1;
    int point_id = 0;
    
    typename std::list<ParetoPoint<nPar, nCrit>*>::iterator p = points.begin();

    for(; p != points.end(); ++p, ++point_id)
    {
        if(!((*p)->satisfaction_bit))
        {
            ++id;
            if(id == not_sat_point_id)
                return point_id;
        }
    }
}

template<int nPar, int nCrit> int PawsAlgorithm<nPar, nCrit>::
                GetNumberOfPoints()
{
    return points.size();
}

template<int nPar, int nCrit> int PawsAlgorithm<nPar, nCrit>::
                GetNumberOfSatisfiedPoints()
{
    int sat_points_num = 0;
    typename std::list<ParetoPoint<nPar, nCrit>*>::iterator p = points.begin();

    for(; p != points.end(); ++p)
        if((*p)->satisfaction_bit)
            ++sat_points_num;
    return sat_points_num;
}

template<int nPar, int nCrit> void PawsAlgorithm<nPar, nCrit>::
                PrintPoints()
{
    std::cerr << std::endl;
    typename std::list<ParetoPoint<nPar, nCrit>*>::iterator p = points.begin();
    
    int num_point = 0;
    for(; p != points.end(); ++p, ++num_point)
    {
        std::cerr << "POINT #" << num_point << ":" << std::endl;
        std::cerr << "PARAMS: ";
        for(int i = 0; i < nPar; ++i)
            std::cerr << (*p)->params[i] << " ";
        std::cerr << std::endl;
        
        std::cerr << "CRITS: ";
        for(int i = 0; i < nCrit; ++i)
            std::cerr << (*p)->crits[i] << " ";
        std::cerr << std::endl;
        
        std::cerr << "DELTA: ";
        std::cerr << (*p)->delta;
        
        std::cerr << std::endl;
        
        std::cerr << std::endl;
    }
}

template<int nPar, int nCrit> int PawsAlgorithm<nPar, nCrit>::
                FindCenterPoint()
{
    int sat_points_num = GetNumberOfSatisfiedPoints();
    int points_num = GetNumberOfPoints();
    int nsat_points_num = points_num - sat_points_num;
    
    typename std::list<ParetoPoint<nPar, nCrit>*>::iterator p = points.begin();
    if(nsat_points_num == 1)
        return GetNotSatisfiedPoint(0);
    else if(nsat_points_num == 2)
        return GetNotSatisfiedPoint(rand() % 2);
    else
    {
        int center_point = GetNotSatisfiedPoint(0);
        int point_id = center_point;
        double max_distance = FindDistance(point_id);
        for(int i = 1; i < nsat_points_num; ++i)
        {
            point_id = GetNotSatisfiedPoint(i);
            double distance = FindDistance(point_id);
            if(distance > max_distance)
            {   
                max_distance = distance;
                center_point = point_id;
            }
        }
        return center_point;
    }  
}

template<int nPar, int nCrit> double PawsAlgorithm<nPar, nCrit>::
                FindDistance(int point_num)
{
    ParetoPoint<nPar, nCrit>* left_point;
    ParetoPoint<nPar, nCrit>* cur_point;
    ParetoPoint<nPar, nCrit>* right_point;
    double left_squared_distance = 0;
    double right_squared_distance = 0;
    double left_distance = 0;
    double right_distance = 0;
    
    // NOTE: Working with first point in set (index = 0)
    if(point_num == 0)
    {
        //return 0; // NOTE: first point can be central
        cur_point = GetPoint(point_num);
        right_point = GetPoint(point_num + 1);
        for(int i = 0; i < nCrit; ++i)
            right_squared_distance += pow(cur_point->crits[i] - right_point->crits[i], 2);
        right_distance = sqrt(right_squared_distance);
    }
    //END OF Working with first point
    
    // NOTE: Working with last point in set (index = num_points - 1)
    if(point_num == GetNumberOfPoints() - 1)
    {
        //return 0; // NOTE: last point can be central
        cur_point = GetPoint(point_num);
        left_point = GetPoint(point_num - 1);
        for(int i = 0; i < nCrit; ++i)
            left_squared_distance += pow(cur_point->crits[i] - left_point->crits[i], 2);
        left_distance = sqrt(left_squared_distance);
    }
    //END OF Working with last point
    
    // NOTE: work out internal points
    cur_point = GetPoint(point_num);
    left_point = GetPoint(point_num - 1);
    right_point = GetPoint(point_num + 1);
    left_squared_distance = 0;
    right_squared_distance = 0;
    left_distance = 0;
    right_distance = 0;
    
    for(int i = 0; i < nCrit; ++i)
        left_squared_distance += pow(cur_point->crits[i] - left_point->crits[i], 2);
    left_distance = sqrt(left_squared_distance);
    for(int i = 0; i < nCrit; ++i)
        right_squared_distance += pow(cur_point->crits[i] - right_point->crits[i], 2);
    right_distance = sqrt(right_squared_distance);
    
    return left_distance + right_distance;
    //END OF work out internal points
}

template<int nPar, int nCrit> void PawsAlgorithm<nPar, nCrit>::
                DeleteOverlapSolutions()
{
    if(points.size() == 2)
        return;
    typename std::list<ParetoPoint<nPar, nCrit>*>::iterator     p = points.begin();
    typename std::list<ParetoPoint<nPar, nCrit>*>::iterator end_p = points.end();
    --end_p;
    int i = 0;
    while(p != end_p)
    {
        ParetoPoint<nPar, nCrit>* cur_point  = *p;
        ParetoPoint<nPar, nCrit>* next_point = *(++p);
        --p;
        if(sqrt(pow(cur_point->crits[0] - next_point->crits[0], 2) +
                pow(cur_point->crits[1] - next_point->crits[1], 2)) < OVERLAP_THRESHOLD)
        {
            points.erase(p++);
        }
        else
            ++p;
        ++i;
    }
}

template<int nPar, int nCrit> void PawsAlgorithm<nPar, nCrit>::
                RestorePoints(int r) // "i want to restore this iterations num"
{
    //! set iter counter
    this->r = r;
    
    //! restore nondom pts
    {
    // form filename
    char fn[30];
    std::ostringstream oss;
    oss << r << ".txt"; // in "human" notation
    strcpy(fn, oss.str().c_str());
    
    // open file
    std::ifstream fin(fn);
    if(!fin)
    {
	std::cerr << "can't open file to restore pts!" << std::endl;
	return;
    }
    int pts_num = 0;
    fin >> pts_num;
    // read pts
    for(int pt_id = 0; pt_id < pts_num; ++pt_id)
    {
	double params[nPar];
	for(int par_id = 0; par_id < nPar; ++par_id)
	    fin >> params[par_id];
	double crits[nCrit];
	for(int crit_id = 0; crit_id < nCrit; ++crit_id)
	    fin >> crits[crit_id];
	double delta;
	fin >> delta;
	bool sat_bit;
	fin >> sat_bit;
	
	ParetoPoint<nPar, nCrit>* par_pt = ParetoPoint<nPar, nCrit>::
					    FormPoint(params, crits, delta, sat_bit);
	points.push_back(par_pt);
    }
    }
    //END OF restore nondom pts
    
    //! restore design pts
    {
    // form filename
    char fn[30];
    std::ostringstream oss;
    oss << r << "_design.txt"; // in "human" notation
    strcpy(fn, oss.str().c_str());
    
    // open file
    std::ifstream fin(fn);
    if(!fin)
    {
	std::cerr << "can't open file to restore pts!" << std::endl;
	return;
    }
    int pts_num = 0;
    fin >> pts_num;
    // read pts
    for(int pt_id = 0; pt_id < pts_num; ++pt_id)
    {
	double params[nPar];
	for(int par_id = 0; par_id < nPar; ++par_id)
	    fin >> params[par_id];
	double crits[nCrit];
	for(int crit_id = 0; crit_id < nCrit; ++crit_id)
	    fin >> crits[crit_id];
	double delta;
	fin >> delta;
	bool sat_bit;
	fin >> sat_bit;
	
	ParetoPoint<nPar, nCrit>* par_pt = ParetoPoint<nPar, nCrit>::
					    FormPoint(params, crits, delta, sat_bit);
	arch_des_pts.push_back(par_pt);
    }
    }
    //END OF restore design pts
    
    //! restore center pts
    {
    // form filename
    char fn[30];
    std::ostringstream oss;
    oss << r << "_center.txt"; // in "human" notation
    strcpy(fn, oss.str().c_str());
    
    // open file
    std::ifstream fin(fn);
    if(!fin)
    {
	std::cerr << "can't open file to restore pts!" << std::endl;
	return;
    }
    int pts_num = 0;
    fin >> pts_num;
    // read pts
    for(int pt_id = 0; pt_id < pts_num; ++pt_id)
    {
	double params[nPar];
	for(int par_id = 0; par_id < nPar; ++par_id)
	    fin >> params[par_id];
	double crits[nCrit];
	for(int crit_id = 0; crit_id < nCrit; ++crit_id)
	    fin >> crits[crit_id];
	double delta;
	fin >> delta;
	bool sat_bit;
	fin >> sat_bit;
	
	ParetoPoint<nPar, nCrit>* par_pt = ParetoPoint<nPar, nCrit>::
					    FormPoint(params, crits, delta, sat_bit);
	arch_centers.push_back(par_pt);
    }
    }
    //END OF restore center pts
    
    //! restore single pts
    {
    // form filename
    char fn[30];
    std::ostringstream oss;
    oss << r << "_single.txt"; // in "human" notation
    strcpy(fn, oss.str().c_str());
    
    // open file
    std::ifstream fin(fn);
    if(!fin)
    {
	std::cerr << "can't open file to restore pts!" << std::endl;
	return;
    }
    int pts_num = 0;
    fin >> pts_num;
    // read pts
    for(int pt_id = 0; pt_id < pts_num; ++pt_id)
    {
	double params[nPar];
	for(int par_id = 0; par_id < nPar; ++par_id)
	    fin >> params[par_id];
	double crits[nCrit];
	for(int crit_id = 0; crit_id < nCrit; ++crit_id)
	    fin >> crits[crit_id];
	double delta;
	fin >> delta;
	bool sat_bit;
	fin >> sat_bit;
	
	ParetoPoint<nPar, nCrit>* par_pt = ParetoPoint<nPar, nCrit>::
					    FormPoint(params, crits, delta, sat_bit);
	arch_opt_single_sols.push_back(par_pt);
    }
    }
    //END OF restore single pts
    
    //! restore complex pts
    {
    // form filename
    char fn[30];
    std::ostringstream oss;
    oss << r << "_complex.txt"; // in "human" notation
    strcpy(fn, oss.str().c_str());
    
    // open file
    std::ifstream fin(fn);
    if(!fin)
    {
	std::cerr << "can't open file to restore pts!" << std::endl;
	return;
    }
    int pts_num = 0;
    fin >> pts_num;
    // read pts
    for(int pt_id = 0; pt_id < pts_num; ++pt_id)
    {
	double params[nPar];
	for(int par_id = 0; par_id < nPar; ++par_id)
	    fin >> params[par_id];
	double crits[nCrit];
	for(int crit_id = 0; crit_id < nCrit; ++crit_id)
	    fin >> crits[crit_id];
	double delta;
	fin >> delta;
	bool sat_bit;
	fin >> sat_bit;
	
	ParetoPoint<nPar, nCrit>* par_pt = ParetoPoint<nPar, nCrit>::
					    FormPoint(params, crits, delta, sat_bit);
	arch_opt_complex_sols.push_back(par_pt);
    }
    }
    //END OF restore single pts
}

template<int nPar, int nCrit> void PawsAlgorithm<nPar, nCrit>::
                SavePoints()
{
    //! save nondom pts
    {
    // form filename
    char fn[30];
    std::ostringstream oss;
    oss << r+1 << ".txt"; // in "human" notation
    strcpy(fn, oss.str().c_str());

    // open file
    std::ofstream fout(fn);
    if(!fout)
    {
	std::cerr << "can't open file to save pts~" << std::endl;
	return;
    }
    // write pts num
    fout << points.size() << std::endl;
    // write pts
    typename std::list<ParetoPoint<nPar, nCrit>*>::iterator p = points.begin();
    while(p != points.end())
    {
	for(int par_id = 0; par_id < nPar; ++par_id)
	    fout << (*p)->params[par_id] << " ";
	fout << std::endl;
	for(int crit_id = 0; crit_id < nCrit; ++crit_id)
	    fout << (*p)->crits[crit_id] << " ";
	fout << std::endl;
	fout << (*p)->delta;
	fout << std::endl;
	fout << ((*p)->satisfaction_bit ? 1 : 0) << std::endl;
	
	++p;
    }
    }
    //END OF save nondom pts
    
    /*//! save design pts
    {
    // form filename
    char fn[30];
    std::ostringstream oss;
    oss << r+1 << "_design.txt"; // in "human" notation
    strcpy(fn, oss.str().c_str());

    // open file
    std::ofstream fout(fn);
    if(!fout)
    {
	std::cerr << "can't open file to save pts~" << std::endl;
	return;
    }
    // write pts num
    fout << arch_des_pts.size() << std::endl;
    // write pts
    typename std::vector<ParetoPoint<nPar, nCrit>*>::iterator p = arch_des_pts.begin();
    while(p != arch_des_pts.end())
    {
	for(int par_id = 0; par_id < nPar; ++par_id)
	    fout << (*p)->params[par_id] << " ";
	fout << std::endl;
	for(int crit_id = 0; crit_id < nCrit; ++crit_id)
	    fout << (*p)->crits[crit_id] << " ";
	fout << std::endl;
	fout << (*p)->delta;
	fout << std::endl;
	fout << ((*p)->satisfaction_bit ? 1 : 0) << std::endl;
	
	++p;
    }
    }
    //END OF save design pts
    
    //! save center pts
    {
    // form filename
    char fn[30];
    std::ostringstream oss;
    oss << r+1 << "_center.txt"; // in "human" notation
    strcpy(fn, oss.str().c_str());

    // open file
    std::ofstream fout(fn);
    if(!fout)
    {
	std::cerr << "can't open file to save pts~" << std::endl;
	return;
    }
    // write pts num
    fout << arch_centers.size() << std::endl;
    // write pts
    typename std::vector<ParetoPoint<nPar, nCrit>*>::iterator p = arch_centers.begin();
    while(p != arch_centers.end())
    {
	for(int par_id = 0; par_id < nPar; ++par_id)
	    fout << (*p)->params[par_id] << " ";
	fout << std::endl;
	for(int crit_id = 0; crit_id < nCrit; ++crit_id)
	    fout << (*p)->crits[crit_id] << " ";
	fout << std::endl;
	fout << (*p)->delta;
	fout << std::endl;
	fout << ((*p)->satisfaction_bit ? 1 : 0) << std::endl;
	
	++p;
    }
    }
    //END OF save center pts
    
    //! save single pts
    {
    // form filename
    char fn[30];
    std::ostringstream oss;
    oss << r+1 << "_single.txt"; // in "human" notation
    strcpy(fn, oss.str().c_str());

    // open file
    std::ofstream fout(fn);
    if(!fout)
    {
	std::cerr << "can't open file to save pts~" << std::endl;
	return;
    }
    // write pts num
    fout << arch_opt_single_sols.size() << std::endl;
    // write pts
    typename std::vector<ParetoPoint<nPar, nCrit>*>::iterator p = arch_opt_single_sols.begin();
    while(p != arch_opt_single_sols.end())
    {
	for(int par_id = 0; par_id < nPar; ++par_id)
	    fout << (*p)->params[par_id] << " ";
	fout << std::endl;
	for(int crit_id = 0; crit_id < nCrit; ++crit_id)
	    fout << (*p)->crits[crit_id] << " ";
	fout << std::endl;
	fout << (*p)->delta;
	fout << std::endl;
	fout << ((*p)->satisfaction_bit ? 1 : 0) << std::endl;
	
	++p;
    }
    }
    //END OF save single pts
    
    //! save complex pts
    {
    // form filename
    char fn[30];
    std::ostringstream oss;
    oss << r+1 << "_complex.txt"; // in "human" notation
    strcpy(fn, oss.str().c_str());

    // open file
    std::ofstream fout(fn);
    if(!fout)
    {
	std::cerr << "can't open file to save pts~" << std::endl;
	return;
    }
    // write pts num
    fout << arch_opt_complex_sols.size() << std::endl;
    // write pts
    typename std::vector<ParetoPoint<nPar, nCrit>*>::iterator p = arch_opt_complex_sols.begin();
    while(p != arch_opt_complex_sols.end())
    {
	for(int par_id = 0; par_id < nPar; ++par_id)
	    fout << (*p)->params[par_id] << " ";
	fout << std::endl;
	for(int crit_id = 0; crit_id < nCrit; ++crit_id)
	    fout << (*p)->crits[crit_id] << " ";
	fout << std::endl;
	fout << (*p)->delta;
	fout << std::endl;
	fout << ((*p)->satisfaction_bit ? 1 : 0) << std::endl;
	
	++p;
    }
    }
    //END OF save complex pts*/
}

template<int nPar, int nCrit> void PawsAlgorithm<nPar, nCrit>::
                FindNonDominatedPoints(typename std::list<ParetoPoint<nPar, nCrit>*>::iterator begin_p,
                                       std::list<ParetoPoint<nPar, nCrit>*>& cont)
{
    // NOTE: find dominated pts in right direction
    typename std::list<ParetoPoint<nPar, nCrit>*>::iterator p = cont.begin();
    ++p;
    int flag1 = 1, flag2 = 0;
    while(p != cont.end())
    {
        for(int i = 0; i < nCrit; ++i)
            if((*begin_p)->crits[i] > (*p)->crits[i])
            {
                flag1 = 0;
                break;
            }
        if(flag1)
        {
            for(int i = 0; i < nCrit; ++i)
                if((*begin_p)->crits[i] < (*p)->crits[i])
                {
                    flag2 = 1;
                    break;
                }
        }
        if(flag2)
            p = cont.erase(p);
        else
            ++p;
        flag1 = 1, flag2 = 0;
    }
    
    // NOTE: find dominated pts in left direction
    typename std::list<ParetoPoint<nPar, nCrit>*>::reverse_iterator rp = find(cont.rbegin(), cont.rend(), *begin_p);
    ++rp;
    flag1 = 1, flag2 = 0;
    while(rp != cont.rend())
    {
        for(int i = 0; i < nCrit; ++i)
            if((*begin_p)->crits[i] > (*rp)->crits[i])
            {
                flag1 = 0;
                break;
            }
        if(flag1)
            for(int i = 0; i < nCrit; ++i)
                if((*begin_p)->crits[i] < (*rp)->crits[i])
                {
                    flag2 = 1;
                    break;
                }
        if(flag2)
            cont.erase(--(rp.base()));
        else
            ++rp;
        flag1 = 1, flag2 = 0;
    }
    
    if(++begin_p != cont.end())
        FindNonDominatedPoints(begin_p, cont);
}

template<int nPar, int nCrit> bool PawsAlgorithm<nPar, nCrit>::
                FitTrustRegion(double new_rad, ParetoPoint<nPar, nCrit>*& tr_reg) // i like it
{
    double left_bounds[nPar], right_bounds[nPar];
    for(int i = 0; i < nPar; ++i)
    {
        left_bounds[i]  = 0;
        right_bounds[i] = 1;
    }
    double  new_center[nPar];
    for(int i = 0; i < nPar; ++i)
        new_center[i] = tr_reg->params[i];
    for(int par_id = 0; par_id < nPar; ++par_id)
    {
        // prove left bound
        double left_delta = new_center[par_id] - left_bounds[par_id];
        if(left_delta < new_rad)
        {
            new_center[par_id] += (new_rad - left_delta);
            // prove right bound (in)
            double right_delta_in = new_center[par_id] - right_bounds[par_id];
            if(right_delta_in > -new_rad)
                return false;
        }
        // prove right bound
        double right_delta = new_center[par_id] - right_bounds[par_id];
        if(right_delta > -new_rad)
        {
            new_center[par_id] -= (new_rad + right_delta);
            // prove left bound (in)
            double left_delta_in = new_center[par_id] - left_bounds[par_id];
            if(left_delta < new_rad)
                return false;
        }
    }
    
    std::cerr << "new center: ";
    for(int par_id = 0; par_id < nPar; ++par_id)
        std::cerr << new_center[par_id] << " ";
    std::cerr << std::endl;
    // assign new vals to NEW point
    tr_reg = ParetoPoint<nPar, nCrit>::FormPoint(new_center, new_rad);
    /*for(int i = 0; i < nPar; ++i)
        tr_reg->params[i] = new_center[i];
    tr_reg->delta = new_rad;*/
    return true;
}

template<int nPar, int nCrit> void PawsAlgorithm<nPar, nCrit>::
                RunAlgorithm(int iter_num)
{
    #ifdef BRUT_HELP
    ParetoBrootForcer<nPar, nCrit> pbf;
    double mesh_bounds[] = {0, 1, 0, 1};
    pbf.AddFunction(functions[0]);
    pbf.AddFunction(functions[1]);
    pbf.DefineMeshBounds(mesh_bounds);
//     pbf.SetStep(BRUT_STEP);
//     pbf.ComputeGrid();
    pbf.ComputeRandomPoints(BRUT_PTS_NUM);
    pbf.FindNonDominatedPoints();
    std::vector<ParetoPoint<nPar, nCrit>*> brut_par_pts;
    pbf.FormPointsToPAWS(brut_par_pts);
    for(int id = 0; id < brut_par_pts.size(); ++id)
        AddPoint(brut_par_pts[id]);
    FindNonDominatedPoints(points.begin(), points);
    #ifdef VERBOSE
    std::cerr << "after brut help: " << std::endl;
    PrintPoints();
    #endif
    #endif
    
    int result;
    while(((result = CenterPointDetermination()) != -1) && (r < iter_num))
    {
        int num_of_points = points.size();
        ParetoPoint<nPar, nCrit>* center_point = GetPoint(result);
        
        #ifdef VERBOSE
        std::cerr << std::endl << "iteration: " << r
                               << "(" << real_r << ")" << std::endl;
        std::cerr << "center_point: #" << result << " [";
        for(int i = 0; i < nPar; ++i)
            std::cerr << center_point->params[i] << " ";
        std::cerr << "] [";
        for(int i = 0; i < nCrit; ++i)
            std::cerr << center_point->crits[i] << " ";
        std::cerr << "]" << std::endl;
        std::cerr << "center_radius:   [" << center_point->delta << "]" << std::endl;
        #else
        std::cerr << "iteration: " << r << std::endl;
        #endif
        
        // NOTE: //! not good!!!
        std::vector<std::vector<std::pair<std::vector<double> ,double > > > global_design_points;
        std::vector<double> mins, maxs;
        std::list<ParetoPoint<nPar, nCrit>*> single_solutions;
        std::list<ParetoPoint<nPar, nCrit>*> complex_solutions;
        // centers of moved trust regions
        // NOTE: must be added before designs pts
        //       not to be erased by center design pts
        std::list<ParetoPoint<nPar, nCrit>*> moved_centers;
        
        // form polyhedron
        double* polyhedron_origin = new double[nPar]; // Origin of polyhedron
        for(int i = 0; i < nPar; ++i)
            polyhedron_origin[i] = center_point->params[i]-(center_point->delta)/4;
        double length = (center_point->delta)/2;
        
        #ifdef APPR
        //TEST CCD (Appr2)
        //TEST local approximation
        //NOTE: approximation changes real functions where #ifdef exists
        ApprProperty<nPar> appr_prop;

        //TODO not good!!! with "center_point->params"
        for(int par_id = 0; par_id < nPar; ++par_id)
            appr_prop.sphere_center[par_id] = center_point->params[par_id];
        appr_prop.sphere_radius = center_point->delta;
        
        #ifndef CCD //NOTE: for NN you must rethink it!
        HyperSphereFunction<nPar>* hyper_sphere_cons = new HyperSphereFunction<nPar>(center_point->params, center_point->delta);
        appr_prop.constrains.push_back(hyper_sphere_cons);
        #endif
	for(int cur_cons = 0; cur_cons < constrains.size(); ++cur_cons)
	    appr_prop.constrains.push_back(constrains[cur_cons]);
        /*for(int c_id = 0; c_id < nPar; ++c_id)
        {
            RightOneParameterConstrain<nPar>* right_wall_cons = new RightOneParameterConstrain<nPar>(1, c_id);
            LeftOneParameterConstrain<nPar>* left_wall_cons = new LeftOneParameterConstrain<nPar>(0, c_id);
        
            appr_prop.constrains.push_back(left_wall_cons);
            appr_prop.constrains.push_back(right_wall_cons);
        }*/
        //////////////////////////
        // NOTE: flag indicating result of approximation learning process
        bool learning_process_flag = false;
        // for TR moving
        double rad_limit = center_point->delta / ro;
        bool moving_bit = false;
        while(center_point->delta > tau)
        {
            std::vector<std::vector<std::pair<std::vector<double> ,double > > > internal_design_points;
	    #ifdef DIBAH
	    //! archive of values of second criterion
	    std::vector<double> ste_vals;
	    #endif
            learning_process_flag = true;
            for(int f_id = 0; f_id < 2; ++f_id)
            {
                appr_prop.function = functions[f_id];
                #ifdef CCD
                ApproximatorCCD<nPar>* appr = new ApproximatorCCD<nPar>(appr_prop);
                #else
                Approximator2<nPar>* appr = new Approximator2<nPar>(appr_prop);
                #endif
                // NOTE: set flag indicating result of approximation learning 
		#ifndef DIBAH
                if(!appr->Learn())
                {
		#else
		if(!appr->Learn(f_id, ste_vals))
                {
		#endif
                    #ifdef TR_MOVING
                    // apply to move TR
                    if(center_point->delta < rad_limit && FitTrustRegion(rad_limit, center_point))
                    {
                        ComputeFunctions(center_point);
                        moved_centers.push_back(center_point);
                        ++real_r;
                        #ifdef VERBOSE
                        std::cerr << std::endl << "iteration: " << r
                                            << "(" << real_r << ")" << std::endl;
                        std::cerr << "center_point(MOVED): #" << result << " [";
                        for(int i = 0; i < nPar; ++i)
                            std::cerr << center_point->params[i] << " ";
                        std::cerr << "] [";
                        for(int i = 0; i < nCrit; ++i)
                            std::cerr << center_point->crits[i] << " ";
                        std::cerr << "]" << std::endl;
                        std::cerr << "center_radius:   [" << center_point->delta << "]" << std::endl;
                        #endif
                        
                        moving_bit = true;
                        rad_limit /= ro;
                        appr_prop.sphere_radius   = center_point->delta;
                        #ifndef CCD //NOTE: for NN you must rethink it!
                        hyper_sphere_cons->radius = center_point->delta;
                        #endif
                        for(int par_id = 0; par_id < nPar; ++par_id)
                        {
                            appr_prop.sphere_center[par_id]  = center_point->params[par_id];
                            #ifndef CCD //NOTE: for NN you must rethink it!
                            hyper_sphere_cons.center[par_id] = center_point->params[par_id];
                            #endif
                        }
                        f_id = -1;
                        continue;
                    }
                    else
                    {
                    center_point->delta /= 1.02;
                    appr_prop.sphere_radius = center_point->delta;
                    #ifndef CCD //NOTE: for NN you must rethink it!
                    hyper_sphere_cons->radius = center_point->delta;
                    #endif
                    
                    learning_process_flag = false;
                    ++real_r;
                    break;
                    }
                    
                    #else //END OF TR_MOVING
                    center_point->delta /= 1.02;
                    appr_prop.sphere_radius = center_point->delta;
                    #ifndef CCD //NOTE: for NN you must rethink it!
                    hyper_sphere_cons->radius = center_point->delta;
                    #endif
                    
                    learning_process_flag = false;
                    ++real_r;
                    break;
                    #endif
                }
                appr_funs[f_id] = appr;
                
                //get design points
                std::vector<std::pair<std::vector<double> ,double > > local_design_points;
                appr->GetDesignPoints(local_design_points);
                
                internal_design_points.push_back(local_design_points);
            }
            
            if(learning_process_flag)
            {
                for(int id = 0; id < internal_design_points.size(); ++id)
                    global_design_points.push_back(internal_design_points[id]);
                break;
            }
            else
            {
                #ifdef VERBOSE
                std::cerr << std::endl << "iteration: " << r
                                    << "(" << real_r << ")" << std::endl;
                std::cerr << "center_point: #" << result << " [";
                for(int i = 0; i < nPar; ++i)
                    std::cerr << center_point->params[i] << " ";
                std::cerr << "] [";
                for(int i = 0; i < nCrit; ++i)
                    std::cerr << center_point->crits[i] << " ";
                std::cerr << "]" << std::endl;
                std::cerr << "center_radius:   [" << center_point->delta << "]" << std::endl;
                #endif
            }
        }

        //! NOTE: if approximator learning process failed
        //!       go to next iteration!!!
        if(!learning_process_flag)
        {
            center_point->satisfaction_bit = true;
            continue;
        }
        #endif
        
        // NOTE: FIND INDIVIDUAL MAXIMUMS OF F1 AND F2
        for(int cur_crit = 0; cur_crit < nCrit; ++cur_crit)
        {
            // create composite function
            CompositeFunction<nPar>* comp_fun = new CompositeFunction<nPar>; // Composite function for penalty algorithm
            #ifdef APPR
            comp_fun->AddFunction(appr_funs[cur_crit], -1);
            #else
            comp_fun->AddFunction(functions[cur_crit], -1);
            #endif
            for(int cur_cons = 0; cur_cons < constrains.size(); ++cur_cons)
            {
                InequalityFunctional<nPar>* cons_functional = new InequalityFunctional<nPar>;
                cons_functional->SetFunction(constrains[cur_cons]);
                comp_fun->AddConstrain(cons_functional, ALPHA);
            }
            HyperSphereFunction<nPar>* hyper_sphere_cons = new HyperSphereFunction<nPar>(center_point->params, center_point->delta);
            InequalityFunctional<nPar>* hyper_sphere_cons_functional = new InequalityFunctional<nPar>;
            hyper_sphere_cons_functional->SetFunction(hyper_sphere_cons);
            comp_fun->AddConstrain(hyper_sphere_cons_functional, ALPHA);
            
            // create method of conditional opt
            #ifdef MSU_COND_OPTIM
            MSU_SlaceTolerAlg<NUM_PAR> sta(ACCURACY, ITMAX, ITMT, MAXK, MAXT,
                        center_point->delta, center_point->params);
            sta.SetCompositeFunction(comp_fun);
            double* optimum = sta.RunAlgorithm();
            #else
            PenaltyAlgorithm<nPar>* pen_alg = new PenaltyAlgorithm<nPar>(polyhedron_origin, length, ACCURACY, ALPHA, CONS_ACCURACY);
            pen_alg->SetCompositeFunction(comp_fun); //setting composite function
            // NOTE: find solution of individual minimum
            double* optimum = pen_alg->RunAlgorithm();      
            #endif
            // NOTE: add optimal point
            ParetoPoint<nPar, nCrit>* opt_par_point = ParetoPoint<nPar, nCrit>::FormPoint(optimum, 0);
            ComputeFunctions(opt_par_point);
            
            #ifdef VERBOSE/*
            // NOTE: output individual minimum
            std::cerr << "ind max#" << cur_crit << ":       [";
            for(int i = 0; i < nPar; ++i)
            std::cerr << opt_par_point->params[i] << " ";
            std::cerr << "] [";
            for(int i = 0; i < nCrit; ++i)
                std::cerr << opt_par_point->crits[i] << " "
                  << "$(" << functions[i]->Compute(opt_par_point->params) << ") ";
            std::cerr << "]" << std::endl;*/
            #endif
            
            maxs.push_back(appr_funs[cur_crit]->Compute(opt_par_point->params));
        }
        //END OF FIND INDIVIDUAL MAXIMUMS OF F1 AND F2
        
        #ifdef VERBOSE
        std::cerr << std::endl;
        #endif
        
        // NOTE: FIND INDIVIDUAL MINIMALS OF F1 AND F2
        for(int cur_crit = 0; cur_crit < nCrit; ++cur_crit)
        {
            // create composite function
            CompositeFunction<nPar>* comp_fun = new CompositeFunction<nPar>; // Composite function for penalty algorithm
            #ifdef APPR
            comp_fun->AddFunction(appr_funs[cur_crit], 1);
            #else
            comp_fun->AddFunction(functions[cur_crit], 1);
            #endif
            for(int cur_cons = 0; cur_cons < constrains.size(); ++cur_cons)
            {
                InequalityFunctional<nPar>* cons_functional = new InequalityFunctional<nPar>;
                cons_functional->SetFunction(constrains[cur_cons]);
                comp_fun->AddConstrain(cons_functional, ALPHA);
            }
            HyperSphereFunction<nPar>* hyper_sphere_cons = new HyperSphereFunction<nPar>(center_point->params, center_point->delta);
            InequalityFunctional<nPar>* hyper_sphere_cons_functional = new InequalityFunctional<nPar>;
            hyper_sphere_cons_functional->SetFunction(hyper_sphere_cons);
            comp_fun->AddConstrain(hyper_sphere_cons_functional, ALPHA);
            
            // create method of conditional opt
            #ifdef MSU_COND_OPTIM
            MSU_SlaceTolerAlg<NUM_PAR> sta(ACCURACY, ITMAX, ITMT, MAXK, MAXT,
                        center_point->delta, center_point->params);
            sta.SetCompositeFunction(comp_fun);
            double* optimum = sta.RunAlgorithm();
            #else
            PenaltyAlgorithm<nPar>* pen_alg = new PenaltyAlgorithm<nPar>(polyhedron_origin, length, ACCURACY, ALPHA, CONS_ACCURACY);
            pen_alg->SetCompositeFunction(comp_fun); //setting composite function
            // NOTE: find solution of individual minimum
            double* optimum = pen_alg->RunAlgorithm();      
            #endif
            // NOTE: add optimal point
            ParetoPoint<nPar, nCrit>* opt_par_point = ParetoPoint<nPar, nCrit>::FormPoint(optimum, 0);
            
            #ifdef VERBOSE
            // NOTE: output individual minimum
            ComputeApprFunctions(opt_par_point);
            std::cerr << "ind min#" << cur_crit << ":       [";
            for(int i = 0; i < nPar; ++i)
            std::cerr << opt_par_point->params[i] << " ";
            std::cerr << "] [";
            for(int i = 0; i < nCrit; ++i)
                std::cerr << opt_par_point->crits[i]  << " ";
//                   << "$(" << functions[i]->Compute(opt_par_point->params) << ") ";
            std::cerr << "]" << std::endl;
            #endif
            
            ComputeFunctions(opt_par_point);
            single_solutions.push_back(opt_par_point);
            mins.push_back(appr_funs[cur_crit]->Compute(opt_par_point->params));
        }
        //END OF FIND INDIVIDUAL MINIMALS OF F1 AND F2

        #ifdef VERBOSE
        std::cerr << std::endl;
        #endif
        
        //! get approximators
        ApproximatorCCD<nPar>* appr_fun0 = static_cast<ApproximatorCCD<nPar>*>(appr_funs[0]);
        ApproximatorCCD<nPar>* appr_fun1 = static_cast<ApproximatorCCD<nPar>*>(appr_funs[1]);
        //! form normalized funs
        std::vector<NormFunction<nPar>*> nappr_funs;
        std::vector<NormFunction<nPar>*> nreal_funs;
        // for convenience
        std::vector<Function<nPar>*> real_funs;
        for(int fun_id = 0; fun_id < nCrit; ++fun_id)
        {
            NormFunction<nPar>* norm_appr = new NormFunction<nPar>(appr_funs[fun_id],
                                                                   mins[fun_id], maxs[fun_id]);
            nappr_funs.push_back(norm_appr);
            // real funs're normalized with appr mins & maxs
            NormFunction<nPar>* norm_real = new NormFunction<nPar>(functions[fun_id],
                                                                   mins[fun_id], maxs[fun_id]);
            nreal_funs.push_back(norm_real);
            real_funs.push_back(functions[fun_id]);
        }
        
        #ifdef OPT_DEBUG
            //! Approximation TESTS
            std::cerr << std::setw(30) << "### TEST(CCD) single ###" << std::endl;
            std::cerr << std::endl;
            std::cerr << std::setw(20) << "PARAMS"
                      << std::setw(28) << "f0 (appr)"
                      << std::setw(21) << "f0 (prec)"
                      << std::setw(21) << "f1 (appr)"
                      << std::setw(22) << "f1 (prec)"
                                       << std::endl;
                       
            double test_params[nPar];
            appr_fun0->_learn_pts[0].GetParams(test_params);
            double real_mins[2], appr_mins[2];
            double real_mins_params[2][nPar], appr_mins_params[2][nPar];
            for(int crit_id = 0; crit_id < 2; ++crit_id)
            {
                real_mins[crit_id] = real_funs[crit_id]->Compute(test_params);
                appr_mins[crit_id] = appr_funs[crit_id]->Compute(test_params);
                for(int par_id = 0; par_id < nPar; ++par_id)
                {
                    real_mins_params[crit_id][par_id] = test_params[par_id];
                    appr_mins_params[crit_id][par_id] = test_params[par_id];
                }
            }
                                       
            int learn_pts_num = appr_fun0->_learn_pts.size();
            for(int learn_point_id = 0; learn_point_id < learn_pts_num; ++learn_point_id)
            {
                appr_fun0->_learn_pts[learn_point_id].GetParams(test_params);
                if(learn_point_id < OUTPUT_LIMIT)
                {
                    std::cerr << std::fixed;
                    std::cerr << std::setw(10) << "point" << std::setw(2) << learn_point_id << ": ";
                    std::cerr << "[";
                    for(int par_id = 0; par_id < nPar; ++par_id)
                        std::cerr << test_params[par_id] << " ";
                    std::cerr << "]";
                    std::cerr << std::setw(12) << std::setprecision(4)  <<  appr_funs[0]->Compute(test_params)
    //                                                      << " (" << nappr_funs[0]->Compute(test_params)     << ")"
                            << std::setw(12) << std::setprecision(4)  <<  real_funs[0]->Compute(test_params)
    //                                                      << " (" << nreal_funs[0]->Compute(test_params)     << ")"                               
                            << std::setw(12) << std::setprecision(4)  <<  appr_funs[1]->Compute(test_params)
    //                                                      << " (" << nappr_funs[1]->Compute(test_params)     << ")"
                            << std::setw(12) << std::setprecision(4)  <<  real_funs[1]->Compute(test_params);
    //                                                      << " (" << nreal_funs[1]->Compute(test_params)     << ")";
                    std::cerr << std::endl;
                }
                
                for(int crit_id = 0; crit_id < 2; ++crit_id)
                {
                    double real_val = real_funs[crit_id]->Compute(test_params);
                    double appr_val = appr_funs[crit_id]->Compute(test_params);
                    if(real_val < real_mins[crit_id])
                    {
                        real_mins[crit_id] = real_val;
                        for(int par_id = 0; par_id < nPar; ++par_id)
                            real_mins_params[crit_id][par_id] = test_params[par_id];
                    }
                    
                    if(appr_val < appr_mins[crit_id])
                    {
                        appr_mins[crit_id] = appr_val;
                        for(int par_id = 0; par_id < nPar; ++par_id)
                            appr_mins_params[crit_id][par_id] = test_params[par_id];
                    }
                }
            }
            std::cerr << std::endl;
            for(int crit_id = 0; crit_id < 2; ++crit_id)
            {
                std::cerr << std::setw(10) << "appr_min" << std::setw(2) << crit_id << ": ";
                std::cerr << "[";
                for(int par_id = 0; par_id < nPar; ++par_id)
                    std::cerr << appr_mins_params[crit_id][par_id] << " ";
                std::cerr << "]";
                std::cerr << std::setw(10) << std::setprecision(4) << appr_mins[crit_id];
                std::cerr << std::endl;
                
                std::cerr << std::setw(10) << "real_min" << std::setw(2) << crit_id << ": ";
                std::cerr << "[";
                for(int par_id = 0; par_id < nPar; ++par_id)
                    std::cerr << real_mins_params[crit_id][par_id] << " ";
                std::cerr << "]";
                std::cerr << std::setw(10) << std::setprecision(4) << real_mins[crit_id];
                std::cerr << std::endl << std::endl;
            }
            std::cerr << std::endl;
        #endif

//      if(r < 1)
//      {
        if(!moving_bit)
        {
        // NOTE: FIND SOLUTIONS OF COMPLEX PROBLEMS
        if(num_of_points == 1)
        {
            PenaltyAlgorithm<nPar>* pen_alg = new PenaltyAlgorithm<nPar>(polyhedron_origin, length, ACCURACY, ALPHA, CONS_ACCURACY);
            CompositeFunction<nPar>* comp_fun = new CompositeFunction<nPar>; // Composite function for penalty algorithm
            for(int fun_id = 0; fun_id < nCrit; ++fun_id)
            {
                #ifdef APPR
                NormFunction<nPar>* normf = nappr_funs[fun_id];
                #else
                NormFunction<nPar>* normf = nreal_funs[fun_id];
                #endif
                comp_fun->AddFunction(normf, 0.5);
            }
            
            for(int cur_cons = 0; cur_cons < constrains.size(); ++cur_cons)
            {
                InequalityFunctional<nPar>* cons_functional = new InequalityFunctional<nPar>;
                cons_functional->SetFunction(constrains[cur_cons]);
                
                comp_fun->AddConstrain(cons_functional, ALPHA);
            }
            
            HyperSphereFunction<nPar>* hyper_sphere_cons = new HyperSphereFunction<nPar>(center_point->params, center_point->delta);
            InequalityFunctional<nPar>* hyper_sphere_cons_functional = new InequalityFunctional<nPar>;
            hyper_sphere_cons_functional->SetFunction(hyper_sphere_cons);
            comp_fun->AddConstrain(hyper_sphere_cons_functional, ALPHA);
            
            #ifdef MSU_COND_OPTIM
            MSU_SlaceTolerAlg<NUM_PAR> sta(ACCURACY, ITMAX, ITMT, MAXK, MAXT,
                        center_point->delta, center_point->params);
            sta.SetCompositeFunction(comp_fun);
            double* optimum = sta.RunAlgorithm();
            #else
            pen_alg->SetCompositeFunction(comp_fun); //setting composite function
            // NOTE: find solution of COMPLEX PROBLEM
            double* optimum = pen_alg->RunAlgorithm();
            #endif
            
            // NOTE: add optimal point
            ParetoPoint<nPar, nCrit>* opt_par_point = ParetoPoint<nPar, nCrit>::FormPoint(optimum, 0);
            
            #ifdef VERBOSE
            // NOTE: output complex minimum
            std::cerr << "complex min" << ":     [";
            for(int i = 0; i < nPar; ++i)
            std::cerr << opt_par_point->params[i] << " ";
            std::cerr << "] [";
            for(int i = 0; i < nCrit; ++i)
                std::cerr << appr_funs[i]->Compute(opt_par_point->params) << " ";
//                   << "$(" << real_funs[i]->Compute(opt_par_point->params) << ") ";
            std::cerr << "]";
            //! output weighted sum
            std::cerr << " " << 0.5*appr_funs[0]->Compute(optimum) + 
                                0.5*appr_funs[1]->Compute(optimum) << " ";
                     /*<< "$(" << 0.5*real_funs[0]->Compute(optimum) + 
                                0.5*real_funs[1]->Compute(optimum) << ") ";*/
            std::cerr << std::endl;
            
            #ifdef OPT_DEBUG
            //! Approximation TESTS
            std::cerr << std::setw(30) << "### TEST(CCD) complex ###" << std::endl;
            std::cerr << std::endl;
            std::cerr << std::setw(20) << "PARAMS"
                      << std::setw(28) << "f0 (appr)"
                      << std::setw(21) << "f0 (prec)"
                      << std::setw(21) << "f1 (appr)"
                      << std::setw(22) << "f1 (prec)"
                      << std::setw(21) << "sum (appr)"
                      << std::setw(21) << "sum (prec)"
                                       << std::endl;
            int learn_pts_num = appr_fun0->_learn_pts.size();
            for(int learn_point_id = 0; learn_point_id < learn_pts_num; ++learn_point_id)
            {
                double test_params[nPar];
                appr_fun0->_learn_pts[learn_point_id].GetParams(test_params);
                
                if(learn_point_id < OUTPUT_LIMIT)
                {
                std::cerr << std::fixed;
                std::cerr << std::setw(10) << "point" << std::setw(2) << learn_point_id << ": ";
                std::cerr << "[";
                for(int par_id = 0; par_id < nPar; ++par_id)
                    std::cerr << test_params[par_id] << " ";
                std::cerr << "]";
                std::cerr << std::setw(12) << std::setprecision(4)  <<  appr_funs[0]->Compute(test_params)
                                                            << " (" << nappr_funs[0]->Compute(test_params)     << ")"
                          << std::setw(12) << std::setprecision(4)  <<  real_funs[0]->Compute(test_params)
                                                            << " (" << nreal_funs[0]->Compute(test_params)     << ")"                               
                          << std::setw(12) << std::setprecision(4)  <<  appr_funs[1]->Compute(test_params)
                                                            << " (" << nappr_funs[1]->Compute(test_params)     << ")"
                          << std::setw(12) << std::setprecision(4)  <<  real_funs[1]->Compute(test_params)
                                                            << " (" << nreal_funs[1]->Compute(test_params)     << ")"
                          << std::setw(12) << std::setprecision(4)  << 0.5* appr_funs[0]->Compute(test_params)
                                                                     + 0.5* appr_funs[1]->Compute(test_params)
                                                            << " (" << 0.5*nappr_funs[0]->Compute(test_params)
                                                                     + 0.5*nappr_funs[1]->Compute(test_params) << ")"
                          << std::setw(12) << std::setprecision(4)  << 0.5* real_funs[0]->Compute(test_params)
                                                                     + 0.5* real_funs[1]->Compute(test_params)
                                                            << " (" << 0.5*nreal_funs[0]->Compute(test_params)
                                                                     + 0.5*nreal_funs[1]->Compute(test_params) << ")";
                std::cerr << std::endl;
                }
            }
            std::cerr << std::endl;
            #endif
            #endif
            
            ComputeFunctions(opt_par_point);
            complex_solutions.push_back(opt_par_point);
        }
        else if((num_of_points == 2) /*|| (result == 0)*/)
        {
            PenaltyAlgorithm<nPar>* pen_alg = new PenaltyAlgorithm<nPar>(polyhedron_origin, length, ACCURACY, ALPHA, CONS_ACCURACY);
            CompositeFunction<nPar>* comp_fun = new CompositeFunction<nPar>; // Composite function for penalty algorithm
            
            // NOTE: preparing fractions
            ParetoPoint<nPar, nCrit>* point1 = GetPoint(0);
            ParetoPoint<nPar, nCrit>* point2 = GetPoint(1);
            
            double wp1 = - point2->crits[1] + point1->crits[1];
            double wp2 =   point2->crits[0] - point1->crits[0];
            double wp_sum = wp1 + wp2;
            wp1 /= wp_sum;
            wp2 /= wp_sum;
            //END OF preparing fractions

            #ifdef APPR
            NormFunction<nPar>* normf0 = nappr_funs[0];
            NormFunction<nPar>* normf1 = nappr_funs[1];
            #else
            NormFunction<nPar>* normf0 = nreal_funs[0];
            NormFunction<nPar>* normf1 = nreal_funs[1];
            #endif
            comp_fun->AddFunction(normf0, wp1);
            comp_fun->AddFunction(normf1, wp2);
            
            for(int cur_cons = 0; cur_cons < constrains.size(); ++cur_cons)
            {
                InequalityFunctional<nPar>* cons_functional = new InequalityFunctional<nPar>;
                cons_functional->SetFunction(constrains[cur_cons]);
                
                comp_fun->AddConstrain(cons_functional, ALPHA);
            }
            
            HyperSphereFunction<nPar>* hyper_sphere_cons = new HyperSphereFunction<nPar>(center_point->params, center_point->delta);
            InequalityFunctional<nPar>* hyper_sphere_cons_functional = new InequalityFunctional<nPar>;
            hyper_sphere_cons_functional->SetFunction(hyper_sphere_cons);
            comp_fun->AddConstrain(hyper_sphere_cons_functional, ALPHA);
            
            #ifdef MSU_COND_OPTIM
            MSU_SlaceTolerAlg<NUM_PAR> sta(ACCURACY, ITMAX, ITMT, MAXK, MAXT,
                        center_point->delta, center_point->params);
            sta.SetCompositeFunction(comp_fun);
            double* optimum = sta.RunAlgorithm();
            #else
            pen_alg->SetCompositeFunction(comp_fun); //setting composite function
            // NOTE: find solution of COMPLEX PROBLEM
            double* optimum = pen_alg->RunAlgorithm();
            #endif
            
            // NOTE: add optimal point
            ParetoPoint<nPar, nCrit>* opt_par_point = ParetoPoint<nPar, nCrit>::FormPoint(optimum, 0);
            
            #ifdef VERBOSE
            // NOTE: output fractions
            std::cerr << "fr#1" << ":     " << wp1 << std::endl;
            std::cerr << "fr#2" << ":     " << wp2 << std::endl;
            
            // NOTE: output complex minimum
            std::cerr << "complex min" << ":     [";
            for(int i = 0; i < nPar; ++i)
            std::cerr << opt_par_point->params[i] << " ";
            std::cerr << "] [";
            for(int i = 0; i < nCrit; ++i)
                std::cerr << appr_funs[i]->Compute(opt_par_point->params) << " ";
//                   << "$(" << real_funs[i]->Compute(opt_par_point->params) << ") ";
            std::cerr << "]";
            //! output weighted sum
            std::cerr << " " << wp1*appr_funs[0]->Compute(optimum) + 
                                wp2*appr_funs[1]->Compute(optimum) << " ";
                     /*<< "$(" << wp1*real_funs[0]->Compute(optimum) + 
                                wp2*real_funs[1]->Compute(optimum) << ") ";*/
            std::cerr << std::endl;
            
            #ifdef OPT_DEBUG
            //! Approximation TESTS
            std::cerr << std::setw(30) << "### TEST(CCD) complex ###" << std::endl;
            std::cerr << std::endl;
            std::cerr << std::setw(20) << "PARAMS"
                      << std::setw(28) << "f0 (appr)"
                      << std::setw(21) << "f0 (prec)"
                      << std::setw(21) << "f1 (appr)"
                      << std::setw(22) << "f1 (prec)"
                      << std::setw(21) << "sum (appr)"
                      << std::setw(21) << "sum (prec)"
                                       << std::endl;
            int learn_pts_num = appr_fun0->_learn_pts.size();
            for(int learn_point_id = 0; learn_point_id < learn_pts_num; ++learn_point_id)
            {
                double test_params[nPar];
                appr_fun0->_learn_pts[learn_point_id].GetParams(test_params);
                
                if(learn_point_id < OUTPUT_LIMIT)
                {
                std::cerr << std::fixed;
                std::cerr << std::setw(10) << "point" << std::setw(2) << learn_point_id << ": ";
                std::cerr << "[";
                for(int par_id = 0; par_id < nPar; ++par_id)
                    std::cerr << test_params[par_id] << " ";
                std::cerr << "]";
                std::cerr << std::setw(12) << std::setprecision(4)  <<  appr_funs[0]->Compute(test_params)
                                                            << " (" << nappr_funs[0]->Compute(test_params)     << ")"
                          << std::setw(12) << std::setprecision(4)  <<  real_funs[0]->Compute(test_params)
                                                            << " (" << nreal_funs[0]->Compute(test_params)     << ")"                               
                          << std::setw(12) << std::setprecision(4)  <<  appr_funs[1]->Compute(test_params)
                                                            << " (" << nappr_funs[1]->Compute(test_params)     << ")"
                          << std::setw(12) << std::setprecision(4)  <<  real_funs[1]->Compute(test_params)
                                                            << " (" << nreal_funs[1]->Compute(test_params)     << ")"
                          << std::setw(12) << std::setprecision(4)  << wp1* appr_funs[0]->Compute(test_params)
                                                                     + wp2* appr_funs[1]->Compute(test_params)
                                                            << " (" << wp1*nappr_funs[0]->Compute(test_params)
                                                                     + wp2*nappr_funs[1]->Compute(test_params) << ")"
                          << std::setw(12) << std::setprecision(4)  << wp1* real_funs[0]->Compute(test_params)
                                                                     + wp2* real_funs[1]->Compute(test_params)
                                                            << " (" << wp1*nreal_funs[0]->Compute(test_params)
                                                                     + wp2*nreal_funs[1]->Compute(test_params) << ")";
                std::cerr << std::endl;
                }
            }
            std::cerr << std::endl;
            #endif
            #endif
            
            ComputeFunctions(opt_par_point);
            complex_solutions.push_back(opt_par_point);
        }
        /*else if((num_of_points > 2) && (result == (num_of_points -1)))
        {
            PenaltyAlgorithm<nPar>* pen_alg = new PenaltyAlgorithm<nPar>(polyhedron_origin, length, ACCURACY, ALPHA, CONS_ACCURACY);
            CompositeFunction<nPar>* comp_fun = new CompositeFunction<nPar>; // Composite function for penalty algorithm
            
            // NOTE: preparing fractions
            ParetoPoint<nPar, nCrit>* point1 = GetPoint(num_of_points - 2);
            ParetoPoint<nPar, nCrit>* point2 = GetPoint(num_of_points - 1);
            
            double wp1 = - point2->crits[1] + point1->crits[1];
            double wp2 =   point2->crits[0] - point1->crits[0];
            double wp_sum = wp1 + wp2;
            wp1 /= wp_sum;
            wp2 /= wp_sum;
            //END OF preparing fractions
            
            #ifdef APPR
            comp_fun->AddFunction(appr_funs[0], wp1);
            comp_fun->AddFunction(appr_funs[1], wp2);
            #else
            comp_fun->AddFunction(functions[0], wp1);
            comp_fun->AddFunction(functions[1], wp2);
            #endif
            
            for(int cur_cons = 0; cur_cons < constrains.size(); ++cur_cons)
            {
                InequalityFunctional<nPar>* cons_functional = new InequalityFunctional<nPar>;
                cons_functional->SetFunction(constrains[cur_cons]);
                comp_fun->AddConstrain(cons_functional, ALPHA);
            }
            
            HyperSphereFunction<nPar>* hyper_sphere_cons = new HyperSphereFunction<nPar>(center_point->params, center_point->delta);
            InequalityFunctional<nPar>* hyper_sphere_cons_functional = new InequalityFunctional<nPar>;
            hyper_sphere_cons_functional->SetFunction(hyper_sphere_cons);
            comp_fun->AddConstrain(hyper_sphere_cons_functional, ALPHA);
            
            #ifdef MSU_COND_OPTIM
            MSU_SlaceTolerAlg<NUM_PAR> sta(ACCURACY, ITMAX, ITMT, MAXK, MAXT,
                        center_point->delta, center_point->params);
            sta.SetCompositeFunction(comp_fun);
            double* optimum = sta.RunAlgorithm();
            #else
            pen_alg->SetCompositeFunction(comp_fun); //setting composite function
            // NOTE: find solution of COMPLEX PROBLEM
            double* optimum = pen_alg->RunAlgorithm();
            #endif
            
            // NOTE: add optimal point
            ParetoPoint<nPar, nCrit>* opt_par_point = ParetoPoint<nPar, nCrit>::FormPoint(optimum, 0);
            ComputeFunctions(opt_par_point); //! appr?
            
            #ifdef VERBOSE
            // NOTE: output complex minimum
            std::cerr << "complex min" << ":     [";
            for(int i = 0; i < nPar; ++i)
            std::cerr << opt_par_point->params[i] << " ";
            std::cerr << "] [";
            for(int i = 0; i < nCrit; ++i)
                std::cerr << opt_par_point->crits[i] << " ";
            std::cerr << "]" << std::endl;
            #endif
            
            solutions.push_back(opt_par_point);
        }*/
        else if(result != 0 && result != (num_of_points -1))
        {
            // NOTE: 'p' - opt problem
            {
            PenaltyAlgorithm<nPar>* pen_alg = new PenaltyAlgorithm<nPar>(polyhedron_origin, length, ACCURACY, ALPHA, CONS_ACCURACY);
            CompositeFunction<nPar>* comp_fun = new CompositeFunction<nPar>; // Composite function for penalty algorithm
            
            // NOTE: preparing fractions
            ParetoPoint<nPar, nCrit>* point1 = GetPoint(result - 1);
            ParetoPoint<nPar, nCrit>* point2 = GetPoint(result);
            
            double wp1 = - point2->crits[1] + point1->crits[1];
            double wp2 =   point2->crits[0] - point1->crits[0];
            double wp_sum = wp1 + wp2;
            wp1 /= wp_sum;
            wp2 /= wp_sum;
            //END OF preparing fractions
            
            #ifdef APPR
            NormFunction<nPar>* normf0 = nappr_funs[0];
            NormFunction<nPar>* normf1 = nappr_funs[1];
            #else
            NormFunction<nPar>* normf0 = nreal_funs[0];
            NormFunction<nPar>* normf1 = nreal_funs[1];
            #endif
            comp_fun->AddFunction(normf0, wp1);
            comp_fun->AddFunction(normf1, wp2);
            
            for(int cur_cons = 0; cur_cons < constrains.size(); ++cur_cons)
            {
                InequalityFunctional<nPar>* cons_functional = new InequalityFunctional<nPar>;
                cons_functional->SetFunction(constrains[cur_cons]);
                comp_fun->AddConstrain(cons_functional, ALPHA);
            }
            
            HyperSphereFunction<nPar>* hyper_sphere_cons = new HyperSphereFunction<nPar>(center_point->params, center_point->delta);
            InequalityFunctional<nPar>* hyper_sphere_cons_functional = new InequalityFunctional<nPar>;
            hyper_sphere_cons_functional->SetFunction(hyper_sphere_cons);
            comp_fun->AddConstrain(hyper_sphere_cons_functional, ALPHA);
            
            #ifdef MSU_COND_OPTIM
            MSU_SlaceTolerAlg<NUM_PAR> sta(ACCURACY, ITMAX, ITMT, MAXK, MAXT,
                        center_point->delta, center_point->params);
            sta.SetCompositeFunction(comp_fun);
            double* optimum = sta.RunAlgorithm();
            #else
            pen_alg->SetCompositeFunction(comp_fun); //setting composite function
            // NOTE: find solution of COMPLEX PROBLEM
            double* optimum = pen_alg->RunAlgorithm();
            #endif
            
            // NOTE: add optimal point
            ParetoPoint<nPar, nCrit>* opt_par_point = ParetoPoint<nPar, nCrit>::FormPoint(optimum, 0);
            
            #ifdef VERBOSE
            // NOTE: output fractions
            std::cerr << "fr#1" << ":     " << wp1 << std::endl;
            std::cerr << "fr#2" << ":     " << wp2 << std::endl;
            
            // NOTE: output complex minimum
            std::cerr << "complex min" << ":     [";
            for(int i = 0; i < nPar; ++i)
            std::cerr << opt_par_point->params[i] << " ";
            std::cerr << "] [";
            for(int i = 0; i < nCrit; ++i)
                std::cerr << appr_funs[i]->Compute(opt_par_point->params) << " ";
//                   << "$(" << real_funs[i]->Compute(opt_par_point->params) << ") ";
            std::cerr << "]";
            //! output weighted sum
            std::cerr << " " << wp1*appr_funs[0]->Compute(optimum) + 
                                wp2*appr_funs[1]->Compute(optimum) << " ";
                     /*<< "$(" << wp1*real_funs[0]->Compute(optimum) + 
                                wp2*real_funs[1]->Compute(optimum) << ") ";*/
            std::cerr << std::endl;
            
            #ifdef OPT_DEBUG
            //! Approximation TESTS
            std::cerr << std::setw(30) << "### TEST(CCD) complex ###" << std::endl;
            std::cerr << std::endl;
            std::cerr << std::setw(20) << "PARAMS"
                      << std::setw(28) << "f0 (appr)"
                      << std::setw(21) << "f0 (prec)"
                      << std::setw(21) << "f1 (appr)"
                      << std::setw(22) << "f1 (prec)"
                      << std::setw(21) << "sum (appr)"
                      << std::setw(21) << "sum (prec)"
                                       << std::endl;
            int learn_pts_num = appr_fun0->_learn_pts.size();
            for(int learn_point_id = 0; learn_point_id < learn_pts_num; ++learn_point_id)
            {
                double test_params[nPar];
                appr_fun0->_learn_pts[learn_point_id].GetParams(test_params);
                
                if(learn_point_id < OUTPUT_LIMIT)
                {
                std::cerr << std::fixed;
                std::cerr << std::setw(10) << "point" << std::setw(2) << learn_point_id << ": ";
                std::cerr << "[";
                for(int par_id = 0; par_id < nPar; ++par_id)
                    std::cerr << test_params[par_id] << " ";
                std::cerr << "]";
                std::cerr << std::setw(12) << std::setprecision(4)  <<  appr_funs[0]->Compute(test_params)
                                                            << " (" << nappr_funs[0]->Compute(test_params)     << ")"
                          << std::setw(12) << std::setprecision(4)  <<  real_funs[0]->Compute(test_params)
                                                            << " (" << nreal_funs[0]->Compute(test_params)     << ")"                               
                          << std::setw(12) << std::setprecision(4)  <<  appr_funs[1]->Compute(test_params)
                                                            << " (" << nappr_funs[1]->Compute(test_params)     << ")"
                          << std::setw(12) << std::setprecision(4)  <<  real_funs[1]->Compute(test_params)
                                                            << " (" << nreal_funs[1]->Compute(test_params)     << ")"
                          << std::setw(12) << std::setprecision(4)  << wp1* appr_funs[0]->Compute(test_params)
                                                                     + wp2* appr_funs[1]->Compute(test_params)
                                                            << " (" << wp1*nappr_funs[0]->Compute(test_params)
                                                                     + wp2*nappr_funs[1]->Compute(test_params) << ")"
                          << std::setw(12) << std::setprecision(4)  << wp1* real_funs[0]->Compute(test_params)
                                                                     + wp2* real_funs[1]->Compute(test_params)
                                                            << " (" << wp1*nreal_funs[0]->Compute(test_params)
                                                                     + wp2*nreal_funs[1]->Compute(test_params) << ")";
                std::cerr << std::endl;
                }
            }
            std::cerr << std::endl;
            #endif
            #endif
            
            ComputeFunctions(opt_par_point);
            complex_solutions.push_back(opt_par_point);
            }
            
            // NOTE: 'q' - opt problem
            {
            PenaltyAlgorithm<nPar>* pen_alg = new PenaltyAlgorithm<nPar>(polyhedron_origin, length, ACCURACY, ALPHA, CONS_ACCURACY);
        
            CompositeFunction<nPar>* comp_fun = new CompositeFunction<nPar>; // Composite function for penalty algorithm
            
            // NOTE: preparing fractions
            ParetoPoint<nPar, nCrit>* point1 = GetPoint(result);
            ParetoPoint<nPar, nCrit>* point2 = GetPoint(result + 1);
            
            double wp1 = - point2->crits[1] + point1->crits[1];
            double wp2 =   point2->crits[0] - point1->crits[0];
            double wp_sum = wp1 + wp2;
            wp1 /= wp_sum;
            wp2 /= wp_sum;
            //END OF preparing fractions
            
            #ifdef APPR
            NormFunction<nPar>* normf0 = nappr_funs[0];
            NormFunction<nPar>* normf1 = nappr_funs[1];
            #else
            NormFunction<nPar>* normf0 = nreal_funs[0];
            NormFunction<nPar>* normf1 = nreal_funs[1];
            #endif
            comp_fun->AddFunction(normf0, wp1);
            comp_fun->AddFunction(normf1, wp2);     
            
            for(int cur_cons = 0; cur_cons < constrains.size(); ++cur_cons)
            {
                InequalityFunctional<nPar>* cons_functional = new InequalityFunctional<nPar>;
                cons_functional->SetFunction(constrains[cur_cons]);
                
                comp_fun->AddConstrain(cons_functional, ALPHA);
            }
            
            HyperSphereFunction<nPar>* hyper_sphere_cons = new HyperSphereFunction<nPar>(center_point->params, center_point->delta);
            InequalityFunctional<nPar>* hyper_sphere_cons_functional = new InequalityFunctional<nPar>;
            hyper_sphere_cons_functional->SetFunction(hyper_sphere_cons);
            comp_fun->AddConstrain(hyper_sphere_cons_functional, ALPHA);
            
            #ifdef MSU_COND_OPTIM
            MSU_SlaceTolerAlg<NUM_PAR> sta(ACCURACY, ITMAX, ITMT, MAXK, MAXT,
                        center_point->delta, center_point->params);
            sta.SetCompositeFunction(comp_fun);
            double* optimum = sta.RunAlgorithm();
            #else
            pen_alg->SetCompositeFunction(comp_fun); //setting composite function
            // NOTE: find solution of COMPLEX PROBLEM
            double* optimum = pen_alg->RunAlgorithm();
            #endif
            
            // NOTE: add optimal point
            ParetoPoint<nPar, nCrit>* opt_par_point = ParetoPoint<nPar, nCrit>::FormPoint(optimum, 0);
            
            #ifdef VERBOSE
            // NOTE: output fractions
            std::cerr << "fr#1" << ":     " << wp1 << std::endl;
            std::cerr << "fr#2" << ":     " << wp2 << std::endl;
            
            // NOTE: output complex minimum
            std::cerr << "complex min" << ":     [";
            for(int i = 0; i < nPar; ++i)
            std::cerr << opt_par_point->params[i] << " ";
            std::cerr << "] [";
            for(int i = 0; i < nCrit; ++i)
                std::cerr << appr_funs[i]->Compute(opt_par_point->params) << " ";
//                   << "$(" << real_funs[i]->Compute(opt_par_point->params) << ") ";
            std::cerr << "]";
            //! output weighted sum
            std::cerr << " " << wp1*appr_funs[0]->Compute(optimum) + 
                                wp2*appr_funs[1]->Compute(optimum) << " ";
                     /*<< "$(" << wp1*real_funs[0]->Compute(optimum) + 
                                wp2*real_funs[1]->Compute(optimum) << ") ";*/
            std::cerr << std::endl;
            
            #ifdef OPT_DEBUG
            //! Approximation TESTS
            std::cerr << std::setw(30) << "### TEST(CCD) complex ###" << std::endl;
            std::cerr << std::endl;
            std::cerr << std::setw(20) << "PARAMS"
                      << std::setw(28) << "f0 (appr)"
                      << std::setw(21) << "f0 (prec)"
                      << std::setw(21) << "f1 (appr)"
                      << std::setw(22) << "f1 (prec)"
                      << std::setw(21) << "sum (appr)"
                      << std::setw(21) << "sum (prec)"
                                       << std::endl;
            int learn_pts_num = appr_fun0->_learn_pts.size();
            for(int learn_point_id = 0; learn_point_id < learn_pts_num; ++learn_point_id)
            {
                double test_params[nPar];
                appr_fun0->_learn_pts[learn_point_id].GetParams(test_params);
                
                if(learn_point_id < OUTPUT_LIMIT)
                {
                std::cerr << std::fixed;
                std::cerr << std::setw(10) << "point" << std::setw(2) << learn_point_id << ": ";
                std::cerr << "[";
                for(int par_id = 0; par_id < nPar; ++par_id)
                    std::cerr << test_params[par_id] << " ";
                std::cerr << "]";
                std::cerr << std::setw(12) << std::setprecision(4)  <<  appr_funs[0]->Compute(test_params)
                                                            << " (" << nappr_funs[0]->Compute(test_params)     << ")"
                          << std::setw(12) << std::setprecision(4)  <<  real_funs[0]->Compute(test_params)
                                                            << " (" << nreal_funs[0]->Compute(test_params)     << ")"                               
                          << std::setw(12) << std::setprecision(4)  <<  appr_funs[1]->Compute(test_params)
                                                            << " (" << nappr_funs[1]->Compute(test_params)     << ")"
                          << std::setw(12) << std::setprecision(4)  <<  real_funs[1]->Compute(test_params)
                                                            << " (" << nreal_funs[1]->Compute(test_params)     << ")"
                          << std::setw(12) << std::setprecision(4)  << wp1* appr_funs[0]->Compute(test_params)
                                                                     + wp2* appr_funs[1]->Compute(test_params)
                                                            << " (" << wp1*nappr_funs[0]->Compute(test_params)
                                                                     + wp2*nappr_funs[1]->Compute(test_params) << ")"
                          << std::setw(12) << std::setprecision(4)  << wp1* real_funs[0]->Compute(test_params)
                                                                     + wp2* real_funs[1]->Compute(test_params)
                                                            << " (" << wp1*nreal_funs[0]->Compute(test_params)
                                                                     + wp2*nreal_funs[1]->Compute(test_params) << ")";
                std::cerr << std::endl;
                }
            }
            std::cerr << std::endl;
            #endif
            #endif
            
            ComputeFunctions(opt_par_point);
            complex_solutions.push_back(opt_par_point);
            }
        }
        //END OF COMPLEX MINIMUM PROBLEMS
        } //FROM 'if(moving_bit)'
//      } //FROM 'if(r < 1)'

        #ifdef END_PTS_OPT
        // NOTE: FIND SOLUTIONS OF IND MIN PROBLEMS
        //       FOR END POINTS
        if(num_of_points > 2)
        {
        for(int end_point_id = 0; end_point_id < num_of_points; end_point_id += (num_of_points - 1))
        {
        ParetoPoint<nPar, nCrit>* end_point = GetPoint(end_point_id);
        //! if this wasn't chosed before
        if(!end_point->delta)
            end_point->delta = delta;
	else if(end_point->delta > this->tau)
	    end_point->delta /= 1.1;
        
        #ifdef VERBOSE
        std::cerr << std::endl << "iteration: " << r
                               << "(" << real_r << ")" << std::endl;
        std::cerr << "end_point: #" << end_point_id << " [";
        for(int i = 0; i < nPar; ++i)
            std::cerr << end_point->params[i] << " ";
        std::cerr << "] [";
        for(int i = 0; i < nCrit; ++i)
            std::cerr << end_point->crits[i] << " ";
        std::cerr << "]" << std::endl;
        std::cerr << "end_radius:   [" << end_point->delta << "]" << std::endl;
        #endif

        // form polyhedron
        double* polyhedron_origin = new double[nPar]; // Origin of polyhedron
        for(int i = 0; i < nPar; ++i)
            polyhedron_origin[i] = end_point->params[i]-(end_point->delta)/4;
        double length = (end_point->delta)/2;
        
        #ifdef APPR
        //TEST CCD (Appr2)
        //TEST local approximation
        //NOTE: approximation changes real functions where #ifdef exists
        ApprProperty<nPar> appr_prop;

        //TODO not good!!! with "center_point->params"
        for(int par_id = 0; par_id < nPar; ++par_id)
            appr_prop.sphere_center[par_id] = end_point->params[par_id];
        appr_prop.sphere_radius = end_point->delta;
        
        #ifndef CCD //NOTE: for NN you must rethink it!
        HyperSphereFunction<nPar>* hyper_sphere_cons = new HyperSphereFunction<nPar>(end_point->params, end_point->delta);
        appr_prop.constrains.push_back(hyper_sphere_cons);
        #endif
        for(int c_id = 0; c_id < nPar; ++c_id)
        {
            RightOneParameterConstrain<nPar>* right_wall_cons = new RightOneParameterConstrain<nPar>(1, c_id);
            LeftOneParameterConstrain<nPar>* left_wall_cons = new LeftOneParameterConstrain<nPar>(0, c_id);
        
            appr_prop.constrains.push_back(left_wall_cons);
            appr_prop.constrains.push_back(right_wall_cons);
        }
        //////////////////////////
        // NOTE: flag indicating result of approximation learning process
        bool learning_process_flag = false;
        while(end_point->delta > tau)
        {
            std::vector<std::vector<std::pair<std::vector<double> ,double > > > internal_design_points;
            learning_process_flag = true;
            for(int f_id = 0; f_id < 2; ++f_id)
            {
                appr_prop.function = functions[f_id];
                #ifdef CCD
                ApproximatorCCD<nPar>* appr = new ApproximatorCCD<nPar>(appr_prop);
                #else
                Approximator2<nPar>* appr = new Approximator2<nPar>(appr_prop);
                #endif
                // NOTE: set flag indicating result of approximation learning 
                if(!appr->Learn())
                {
                    end_point->delta /= 1.02;
                    appr_prop.sphere_radius = end_point->delta;
                    #ifndef CCD //NOTE: for NN you must rethink it!
                    hyper_sphere_cons->radius = end_point->delta;
                    #endif
                    
                    learning_process_flag = false;
                    ++real_r;
                    break;
                }
                appr_funs[f_id] = appr;
                
                //get design points
                std::vector<std::pair<std::vector<double> ,double > > local_design_points;
                appr->GetDesignPoints(local_design_points);
                
                internal_design_points.push_back(local_design_points);
            }
            
            if(learning_process_flag)
            {
                for(int id = 0; id < internal_design_points.size(); ++id)
                    global_design_points.push_back(internal_design_points[id]);
                break;
            }
            else
            {
                #ifdef VERBOSE
                std::cerr << std::endl << "iteration: " << r
                                    << "(" << real_r << ")" << std::endl;
                std::cerr << "end_point: #" << result << " [";
                for(int i = 0; i < nPar; ++i)
                    std::cerr << end_point->params[i] << " ";
                std::cerr << "] [";
                for(int i = 0; i < nCrit; ++i)
                    std::cerr << end_point->crits[i] << " ";
                std::cerr << "]" << std::endl;
                std::cerr << "end_radius:   [" << end_point->delta << "]" << std::endl;
                #endif
            }
        }

        //! NOTE: if approximator learning process failed
        //!       go to next iteration!!!
        if(!learning_process_flag)
        {
            end_point->satisfaction_bit = true;
            continue;
        }
        #endif
        
        // NOTE: FIND INDIVIDUAL MINIMALS OF F1 AND F2
        for(int cur_crit = 0; cur_crit < nCrit; ++cur_crit)
        {
            // create composite function
            CompositeFunction<nPar>* comp_fun = new CompositeFunction<nPar>; // Composite function for penalty algorithm
            #ifdef APPR
            comp_fun->AddFunction(appr_funs[cur_crit], 1);
            #else
            comp_fun->AddFunction(functions[cur_crit], 1);
            #endif
            for(int cur_cons = 0; cur_cons < constrains.size(); ++cur_cons)
            {
                InequalityFunctional<nPar>* cons_functional = new InequalityFunctional<nPar>;
                cons_functional->SetFunction(constrains[cur_cons]);
                comp_fun->AddConstrain(cons_functional, ALPHA);
            }
            HyperSphereFunction<nPar>* hyper_sphere_cons = new HyperSphereFunction<nPar>(end_point->params, end_point->delta);
            InequalityFunctional<nPar>* hyper_sphere_cons_functional = new InequalityFunctional<nPar>;
            hyper_sphere_cons_functional->SetFunction(hyper_sphere_cons);
            comp_fun->AddConstrain(hyper_sphere_cons_functional, ALPHA);
            
            // create method of conditional opt
            #ifdef MSU_COND_OPTIM
            MSU_SlaceTolerAlg<NUM_PAR> sta(ACCURACY, ITMAX, ITMT, MAXK, MAXT,
                        end_point->delta, end_point->params);
            sta.SetCompositeFunction(comp_fun);
            double* optimum = sta.RunAlgorithm();
            #else
            PenaltyAlgorithm<nPar>* pen_alg = new PenaltyAlgorithm<nPar>(polyhedron_origin, length, ACCURACY, ALPHA, CONS_ACCURACY);
            pen_alg->SetCompositeFunction(comp_fun); //setting composite function
            // NOTE: find solution of individual minimum
            double* optimum = pen_alg->RunAlgorithm();      
            #endif
            // NOTE: add optimal point
            ParetoPoint<nPar, nCrit>* opt_par_point = ParetoPoint<nPar, nCrit>::FormPoint(optimum, 0);
            
            #ifdef VERBOSE
            // NOTE: output individual minimum
            ComputeApprFunctions(opt_par_point);
            std::cerr << "ind min#" << cur_crit << ":       [";
            for(int i = 0; i < nPar; ++i)
            std::cerr << opt_par_point->params[i] << " ";
            std::cerr << "] [";
            for(int i = 0; i < nCrit; ++i)
                std::cerr << opt_par_point->crits[i]  << " ";
//                   << "$(" << functions[i]->Compute(opt_par_point->params) << ") ";
            std::cerr << "]" << std::endl;
            #endif
            
            ComputeFunctions(opt_par_point);
            single_solutions.push_back(opt_par_point);
        }
        //END OF FIND INDIVIDUAL MINIMALS OF F1 AND F2

        #ifdef VERBOSE
        std::cerr << std::endl;
        #endif
        }
        }
        #endif
        
        //! add moved points
        #ifdef TR_MOVING
        typename std::list<ParetoPoint<nPar, nCrit>*>::iterator p1 = moved_centers.begin();
        for(; p1 != moved_centers.end(); ++p1)
            if(IsPointListed(*p1))
                AddPoint(*p1);
        #endif
        
        //! set design points
        #ifdef ADD_DESIGN_POINTS
        #ifdef APPR
        //! NOTE: only if approximator learning succeeded
        if(learning_process_flag)
            SetDesignPoints(global_design_points, this->delta);
        #else
            SetDesignPoints(result, GetPoint(result)->delta);
	//TODO: add design pts for end pts
            SetDesignPoints(0, GetPoint(0)->delta);
            SetDesignPoints(points.size()-1, GetPoint(points.size()-1)->delta);
        #endif
        #endif
        
        //! find non-dom solutions among opr problems results
        //! using appr functions
//      FindNonDominatedPoints(solutions.begin(), solutions);
            
        //! measure solutions & add to archive
        typename std::list<ParetoPoint<nPar, nCrit>*>::iterator p = single_solutions.begin();
        for(; p != single_solutions.end(); ++p)
        {
            //NOTE: points like (-0.1; 0.1) are deleted
            //      with help of ProveConstrains()
            if(IsPointListed(*p) && !ProveConstrains(*p))
                AddPoint(*p);
            //! copy pts to archive of opt solutions
            AddPoint(*p, arch_opt_single_sols);
        }
        
        p = complex_solutions.begin();
        for(; p != complex_solutions.end(); ++p)
        {
            if(IsPointListed(*p) && !ProveConstrains(*p))
                AddPoint(*p);
            AddPoint(*p, arch_opt_complex_sols);
        }
        //! copy center point to archive of center points
        AddPoint(center_point, arch_centers);
            
        FindNonDominatedPoints(points.begin(), points);
        SortPoints();
        DeleteOverlapSolutions();
        
        #ifdef VERBOSE
//      std::cerr << std::endl << "after finding non-dominated solutions:";
	SavePoints();
        PrintPoints();
        #endif
        ++r, ++real_r;
    }
    
    // TODO:
    // finish cleaning to get off from "out range" points
    // whose existence is because of tolerance on constrains
    // in PenaltyAlgorithm
//     CleanPoints();

    //! output of final result
    PrintPoints();
}

template<int nPar, int nCrit> void PawsAlgorithm<nPar, nCrit>::
                CleanPoints()
{
    // NOTE: now this concerns only pts
    //       out of constraining polyhedron
    
    // NOTE: for this time only :)
    int num_of_points_to_prove = 20;
    
    typename std::list<ParetoPoint<nPar, nCrit>*>::iterator p = points.begin();
    int pt_id = 0;
    while(pt_id < num_of_points_to_prove && p != points.end())
    {
        if(ProveConstrains(*p))
            p = points.erase(p);
        else
            ++p;
        ++pt_id;
    }
}

template<int nPar, int nCrit> int PawsAlgorithm<nPar, nCrit>::
                IsPointListed(ParetoPoint<nPar, nCrit>* par_point)
{ 
    typename std::list<ParetoPoint<nPar, nCrit>*>::iterator p = points.begin();    
    for(; p != points.end(); ++p)
    {   
        int flag = 0;
        for(int i = 0; i < nPar; ++i)
            if((*p)->params[i] != par_point->params[i])
            {
                flag = 1;
                break;
            }
        if(!flag)
        {
            //good
            return 0;
        }
    }
    return 1;
}

template<int nPar, int nCrit> int PawsAlgorithm<nPar, nCrit>::
                CenterPointDetermination()
{
    // NOTE: Flag: 1 - found center_point
    //            -1 - nothing found
    int flag = 0;
    int center_point = 0;
    
    while(!flag)
    {   
        double delta = this->delta;
        center_point =  FindCenterPoint();
        ParetoPoint<nPar, nCrit>* center_par_point = GetPoint(center_point);
        
        while(true)
        {
            if((center_par_point->delta > 0) && (center_par_point->delta <= delta))
            {
                center_par_point->delta /= this->ro;
                if(center_par_point->delta < this->tau)
                {
                    center_par_point->satisfaction_bit = true;   
                    if(GetNumberOfPoints() == GetNumberOfSatisfiedPoints())
                    {
                        flag = -1;
                        break;
                    }
                    else
                        break;
                }
                else
                {
                    flag = 1;
                    break;
                }
            }
            else
            {
                center_par_point->delta = delta;
                flag = 1;
                break;
            }
        }
    }
    if(flag < 0)
        return -1;
    else
        return center_point;
}

#endif